module gitee.com/zfd81/magic

go 1.22

toolchain go1.22.6

require (
	gitee.com/zfd81/rooster v1.0.3
	github.com/denisenkom/go-mssqldb v0.12.3
	github.com/dop251/goja v0.0.0-20240828124009-016eb7256539
	github.com/go-sql-driver/mysql v1.8.1
	github.com/hpcloud/tail v1.0.0
	github.com/jlaffaye/ftp v0.2.0
	github.com/lib/pq v1.10.7
	github.com/pkg/sftp v1.13.6
	github.com/spf13/cast v1.7.0
	golang.org/x/crypto v0.23.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/antonmedv/expr v1.8.9 // indirect
	github.com/dlclark/regexp2 v1.11.4 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/google/pprof v0.0.0-20230207041349-798e818bf904 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
