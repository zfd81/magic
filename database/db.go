package database

import (
	"fmt"
	"gitee.com/zfd81/magic/file"
	"gitee.com/zfd81/magic/util"
	js "github.com/dop251/goja"
	"github.com/spf13/cast"
	"strings"
	"time"

	"gitee.com/zfd81/rooster/rsql"
	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

type MagicDB struct {
	vm       *js.Runtime
	db       *rsql.DB
	PageSize int
}

func (d *MagicDB) QueryOne(query string, arg interface{}) (map[string]interface{}, error) {
	sql := strings.TrimSpace(query) //获取SQL
	if sql == "" {
		return nil, fmt.Errorf("sql statement cannot be empty")
	}
	return d.db.QueryMap(sql, arg)
}

func (d *MagicDB) QueryOneFunc(query string, arg interface{}) map[string]interface{} {
	m, err := d.QueryOne(query, arg)
	if err != nil {
		panic(d.vm.ToValue(err))
	}
	return m
}

func (d *MagicDB) Query(query string, arg interface{}, pageNumber int, pageSize int) ([]map[string]interface{}, error) {
	sql := strings.TrimSpace(query) //获取SQL
	if sql == "" {
		return nil, fmt.Errorf("sql statement cannot be empty")
	}
	if pageNumber > 0 {
		if pageSize < 1 {
			pageSize = d.PageSize
		}
		return d.db.QueryMapList(sql, arg, pageNumber, pageSize)
	} else {
		rs, err := d.db.Query(sql, arg)
		if err != nil {
			return nil, err
		}
		return rs.MapListScan()
	}
}

func (d *MagicDB) QueryFunc(query string, arg interface{}, pageNumber int, pageSize int) []map[string]interface{} {
	l, err := d.Query(query, arg, pageNumber, pageSize)
	if err != nil {
		panic(d.vm.ToValue(err))
	}
	return l
}

func (d *MagicDB) Save(table string, arg interface{}) (int64, error) {
	table = strings.TrimSpace(table)
	if table == "" {
		return -1, fmt.Errorf("table name cannot be empty")
	}
	if arg == nil {
		return -1, fmt.Errorf("parameter cannot be empty")
	}
	m, ok := arg.(map[string]interface{})
	if ok {
		return d.db.Save(m, table)
	} else {
		l, ok := arg.([]interface{})
		if ok {
			return d.db.BatchSave(l, table)
		} else {
			l, ok := arg.([]map[string]interface{})
			if ok {
				return d.db.BatchSave(util.Map(l, func(item map[string]interface{}, index int) interface{} {
					return item
				}), table)
			} else {
				return -1, fmt.Errorf("parameter data type error")
			}
		}
	}
}

func (d *MagicDB) SaveFunc(table string, arg interface{}) int64 {
	cnt, err := d.Save(table, arg)
	if err != nil {
		panic(d.vm.ToValue(err))
	}
	return cnt
}

func (d *MagicDB) Exec(query string, arg interface{}) (int64, error) {
	sql := strings.TrimSpace(query) //获取SQL
	if sql == "" {
		return -1, fmt.Errorf("sql statement cannot be empty")
	}
	return d.db.Exec(sql, arg)
}

func (d *MagicDB) ExecFunc(query string, arg interface{}) int64 {
	arr, ok := arg.([]interface{})
	if ok {
		ml := util.Map(arr, func(item interface{}, index int) map[string]interface{} {
			m, ok := item.(map[string]interface{})
			if ok {
				return m
			}
			return nil
		})
		if len(ml) > 0 {
			arg = ml
		}
	}

	sql := strings.TrimSpace(query) //获取SQL
	cnt, err := d.Exec(sql, arg)
	if err != nil {
		panic(d.vm.ToValue(err))
	}
	return cnt
}

func (d *MagicDB) WriteFile(query string, arg interface{}, name, sep string) (cnt int, e error) {
	defer func() {
		if r := recover(); r != nil {
			e = fmt.Errorf(cast.ToString(r))
		}
	}()
	sql := strings.TrimSpace(query) //获取SQL
	if sql == "" {
		return 0, fmt.Errorf("sql statement cannot be empty")
	}
	f, err := file.NewFile(name)
	if err != nil {
		return 0, err
	}
	defer f.Close()
	e = d.db.Read(sql, arg, func(row map[string]interface{}) error {
		i := 0
		for _, field := range row {
			if i > 0 {
				_, err = f.WriteString(sep + cast.ToString(field))
			} else {
				_, err = f.WriteString(cast.ToString(field))
			}
			if err != nil {
				return err
			}
			i++
		}
		f.WriteString("\n")
		cnt++
		return nil
	})
	return
}

func (d *MagicDB) WriteFileFunc(query string, arg interface{}, name, sep string) int {
	cnt, err := d.WriteFile(query, arg, name, sep)
	if err != nil {
		panic(d.vm.ToValue(err))
	}
	return cnt
}

func (d *MagicDB) SetMaxOpenConns(n int) {
	d.db.SetMaxOpenConns(n)
}

func (d *MagicDB) SetMaxIdleConns(n int) {
	d.db.SetMaxIdleConns(n)
}

func (d *MagicDB) SetConnMaxIdleTime(second time.Duration) {
	d.db.SetConnMaxIdleTime(second * time.Second)
}

func (d *MagicDB) SetConnMaxLifetime(second time.Duration) {
	d.db.SetConnMaxLifetime(second * time.Second)
}

func NewDB(vm *js.Runtime, db *rsql.DB) *MagicDB {
	return &MagicDB{
		vm:       vm,
		db:       db,
		PageSize: 20,
	}
}
