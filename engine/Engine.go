package engine

type Magic interface {
	RootPath() string
	AbsPath(path string) string
	ThrowException(format string, msgs ...interface{})
}
