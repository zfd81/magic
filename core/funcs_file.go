package core

import (
	"gitee.com/zfd81/magic/file"
)

func Copy(e *Engine) func(string, string) {
	return func(src, dest string) {
		_, err := file.Copy(src, dest)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func Rename(e *Engine) func(string, string) {
	return func(oldpath, newname string) {
		err := file.Rename(oldpath, newname)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func Remove(e *Engine) func(string) {
	return func(path string) {
		err := file.Remove(path)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func Mkdir(e *Engine) func(string) {
	return func(name string) {
		err := file.Mkdir(name)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func Ls(e *Engine) func(string) []map[string]interface{} {
	return func(name string) []map[string]interface{} {
		files, err := file.Ls(name)
		if err != nil {
			e.ThrowException(err.Error())
		}
		return files
	}
}

func ReadFile(e *Engine) func(string) string {
	return func(name string) string {
		bytes, err := file.ReadFile(name)
		if err != nil {
			e.ThrowException(err.Error())
		}
		return string(bytes)
	}
}

func ReadFileByLine(e *Engine) func(name string, handler func(line string) bool) {
	return func(name string, handler func(line string) bool) {
		err := file.ReadFileByLine(name, handler)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func NewFile(e *Engine) func(string) *file.File {
	return func(name string) *file.File {
		f, err := file.NewFile(name)
		if err != nil {
			e.ThrowException(err.Error())
		}
		return f
	}
}

func NewFtp(e *Engine) func(string, string, string) *file.Ftp {
	return func(addr, user, password string) *file.Ftp {
		ftp, err := file.NewFtp(addr, user, password)
		if err != nil {
			e.ThrowException(err.Error())
		}
		return ftp
	}
}

func NewSftp(e *Engine) func(string, string, string) *file.Sftp {
	return func(addr, user, password string) *file.Sftp {
		sftp, err := file.NewSftp(addr, user, password)
		if err != nil {
			e.ThrowException(err.Error())
		}
		return sftp
	}
}

func TailFile(e *Engine) func(string, func(line string) bool, bool) {
	return func(name string, handler func(line string) bool, mustExist bool) {
		err := file.TailFile(name, handler, mustExist)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}
