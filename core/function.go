package core

import (
	"errors"
	"fmt"
	"gitee.com/zfd81/magic/util"
	js "github.com/dop251/goja"
	"github.com/spf13/cast"
)

type Function interface {
	Name() string
	Perform(args ...interface{}) (interface{}, error)
}

type MagicFunction struct {
	vm       *js.Runtime
	name     string
	function js.Callable
}

func (f MagicFunction) Name() string {
	return f.name
}

func (f MagicFunction) Perform(args ...interface{}) (v interface{}, e error) {
	defer func() {
		if r := recover(); r != nil {
			e = errors.New(cast.ToString(r))
		}
	}()

	params := util.Map(args, func(item interface{}, index int) js.Value {
		return f.vm.ToValue(item)
	})

	value, err := f.function(js.Undefined(), params...)
	if err != nil {
		return nil, fmt.Errorf("function %s perform error: %v", f.name, err)
	}

	val, err := convertValue(value)
	if err != nil {
		return nil, fmt.Errorf("function %s perform error: %v", f.name, err)
	}
	return val, nil
}

func NewFunction(vm *js.Runtime, name string, function js.Callable) Function {
	return &MagicFunction{
		vm:       vm,
		name:     name,
		function: function,
	}
}
