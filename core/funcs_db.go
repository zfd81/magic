package core

import (
	"fmt"
	"gitee.com/zfd81/magic/database"
	"gitee.com/zfd81/rooster/rsql"
	"strings"
)

func NewDB(e *Engine) func(string, string, uint32, string, string, string) *database.MagicDB {
	return func(driver, host string, port uint32, user, password, dbName string) *database.MagicDB {
		var driverName, dsn string
		switch strings.ToLower(driver) {
		case "mysql":
			driverName = "mysql"
			dsn = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=true&loc=Local", user, password, host, port, dbName)
		case "mssql":
			driverName = "mssql"
			dsn = fmt.Sprintf("server=%s;user id=%s;password=%s;database=%s;port=%d", host, user, password, dbName, port)
		case "postgresql":
			driverName = "postgres"
			dsn = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbName)
		}
		db, err := rsql.Open(driverName, dsn)
		if err != nil {
			e.ThrowException(err.Error())
		}
		return database.NewDB(e.vm, db)
	}
}

func OpenDB(e *Engine) func(string) *database.MagicDB {
	return func(dsName string) *database.MagicDB {
		if e.ctx == nil {
			return nil
		}
		db := e.ctx.SelectDataSource(dsName)
		if db == nil {
			return nil
		}
		return database.NewDB(e.vm, db)
	}
}
