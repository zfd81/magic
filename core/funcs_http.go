package core

import (
	"fmt"
	"net/http"
	"strings"

	"gitee.com/zfd81/magic/httpclient"
)

func HttpGet(e *Engine) func(string, map[string]interface{}, map[string]interface{}) *httpclient.Response {
	return func(url string, param, header map[string]interface{}) *httpclient.Response {
		if err := CheckUrl(url); err != nil {
			e.ThrowException(err.Error())
		}

		resp, err := httpclient.Get(url, param, ToHeader(header))
		if err != nil {
			resp.Data = err.Error()
		}
		return resp
	}
}

func HttpPost(e *Engine) func(string, map[string]interface{}, map[string]interface{}) *httpclient.Response {
	return func(url string, param, header map[string]interface{}) *httpclient.Response {
		if err := CheckUrl(url); err != nil {
			e.ThrowException(err.Error())
		}

		resp, err := httpclient.Post(url, param, ToHeader(header))
		if err != nil {
			resp.Data = err.Error()
		}
		return resp
	}
}

func HttpDelete(e *Engine) func(string, map[string]interface{}, map[string]interface{}) *httpclient.Response {
	return func(url string, param, header map[string]interface{}) *httpclient.Response {
		if err := CheckUrl(url); err != nil {
			e.ThrowException(err.Error())
		}

		resp, err := httpclient.Delete(url, param, ToHeader(header))
		if err != nil {
			resp.Data = err.Error()
		}
		return resp
	}
}

func HttpPut(e *Engine) func(string, map[string]interface{}, map[string]interface{}) *httpclient.Response {
	return func(url string, param, header map[string]interface{}) *httpclient.Response {
		if err := CheckUrl(url); err != nil {
			e.ThrowException(err.Error())
		}

		resp, err := httpclient.Put(url, param, ToHeader(header))
		if err != nil {
			resp.Data = err.Error()
		}
		return resp
	}
}

func RespWrite(e *Engine) func(data, header interface{}) {
	return func(data, header interface{}) {
		resp := e.ctx.Response()
		resp.SetData(data)
		if header != nil {
			val, ok := header.(map[string]interface{})
			if ok {
				resp.Header = httpclient.Header{}
				for k, v := range val {
					resp.AddHeader(k, v)
				}
			}
		}
		resp.SetStatusCode(http.StatusOK)
	}
}

func RespError(e *Engine) func(int, interface{}) {
	return func(statusCode int, data interface{}) {
		resp := e.ctx.Response()
		resp.SetData(data)
		if statusCode < 100 {
			resp.SetStatusCode(http.StatusInternalServerError)
			e.ThrowException("Status code error: %d", statusCode)
		}
		resp.SetStatusCode(statusCode)
	}
}

func CheckUrl(url string) error {
	url = strings.TrimSpace(url)
	if url == "" {
		return fmt.Errorf("url cannot be empty")
	}
	return nil
}

func ToHeader(header map[string]interface{}) httpclient.Header {
	h := httpclient.Header{}
	for k, v := range header {
		h.Set(k, v)
	}
	return h
}
