package core

import (
	"fmt"
	js "github.com/dop251/goja"
	"github.com/spf13/cast"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func Require(e *Engine) func(args ...string) js.Value {
	root, err := e.GetVar("__magic_root")
	if err != nil {
		e.ThrowException(err.Error())
	}
	return func(args ...string) js.Value {
		if len(args) < 1 {
			e.ThrowException(fmt.Sprintf("require parameter cannot be empty."))
		}
		var dir, location string
		if len(args) == 1 {
			dir = cast.ToString(root)
			location = args[0]
		} else {
			dir = args[0]
			location = args[1]
		}
		location = strings.TrimSpace(location)
		if !strings.HasSuffix(location, ".js") {
			location = location + ".js"
		}
		var fullPath string
		if strings.HasPrefix(path.Dir(location), "/") {
			fullPath = filepath.Join(cast.ToString(root), location)
		} else {
			fullPath = filepath.Join(cast.ToString(dir), location)
		}

		if e.GetModule(fullPath) == nil {
			info, err := os.Stat(fullPath)
			if err != nil || info.IsDir() {
				e.ThrowException(fmt.Sprintf("module '%s' not found.", location))
			}

			bytes, err := os.ReadFile(fullPath)
			if err != nil {
				e.ThrowException("read module %s failed: %v \n", location, err)
			}

			jsModule := e.GetRuntime().NewObject()
			jsModule.Set("exports", e.GetRuntime().NewObject())
			newDir, _ := filepath.Split(fullPath)
			jsModule.Set("__currentDir", e.GetRuntime().ToValue(newDir))

			source := fmt.Sprintf("(function(exports, currentRequire, module) {var require=function(location){return currentRequire(module.__currentDir, location)}; %s \n})", string(bytes))
			parsed, err := js.Parse("", source)
			if err != nil {
				e.ThrowException(err.Error())
			}
			prg, err := js.CompileAST(parsed, false)
			if err != nil {
				e.ThrowException(err.Error())
			}

			f, err := e.GetRuntime().RunProgram(prg)
			if err != nil {
				e.ThrowException(err.Error())
			}

			if call, ok := js.AssertFunction(f); ok {
				jsExports := jsModule.Get("exports")
				jsRequire := e.GetRuntime().Get("require")
				_, err = call(jsExports, jsExports, jsRequire, jsModule)
				if err != nil {
					e.ThrowException(err.Error())
				}
				e.AddModule(fullPath, jsModule.Get("exports"))
			} else {
				e.ThrowException("Invalid module")
			}
		}
		return e.GetModule(fullPath)
	}
}
