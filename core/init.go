package core

import (
	"gitee.com/zfd81/magic/core/funcs/file"
	"gitee.com/zfd81/magic/core/funcs/str"
	"gitee.com/zfd81/magic/core/funcs/yaml"
)

func InitializeSystem(se *Engine) *Engine {
	//参数
	se.AddFunc("__args", []string{})

	//系统函数
	se.AddFunc("scan", Scan(se))
	se.AddFunc("print", Print(se))
	se.AddFunc("println", Println(se))
	se.AddFunc("printf", Printf(se))
	se.AddFunc("sleep", Sleep)
	se.AddFunc("__sys_execute", Execute)

	//httpclient函数
	se.AddFunc("__http_get", HttpGet(se))
	se.AddFunc("__http_post", HttpPost(se))
	se.AddFunc("__http_delete", HttpDelete(se))
	se.AddFunc("__http_put", HttpPut(se))
	se.AddFunc("__resp_write", RespWrite(se))
	se.AddFunc("__resp_error", RespError(se))

	//字符串函数
	se.AddFunc("__string_left", str.Left)
	se.AddFunc("__string_right", str.Right)
	se.AddFunc("__string_lpad", str.Lpad)
	se.AddFunc("__string_rpad", str.Rpad)
	//se.AddFunc("startsWith", str.StartsWith)
	//se.AddFunc("endsWith", str.EndsWith)
	//se.AddFunc("__string_split", str.Split)
	se.AddFunc("__string_splitn", str.SplitN)
	se.AddFunc("__string_splitms", str.SplitMS)
	//se.AddFunc("__string_replaceAll", str.ReplaceAll)
	se.AddFunc("join", str.Join(se))
	se.AddFunc("__string_match", str.MatchString(se))

	//文件操作函数
	//se.AddFunc("__file_copy", Copy(se))
	//se.AddFunc("__file_rename", Rename(se))
	//se.AddFunc("__file_remove", Remove(se))
	//se.AddFunc("__file_mkdir", Mkdir(se))
	//se.AddFunc("__file_ls", Ls(se))
	//se.AddFunc("__file_readfile", ReadFile(se))
	//se.AddFunc("__file_readfilebyline", ReadFileByLine(se))
	se.AddFunc("__file_newfile", file.NewFile(se))
	se.AddFunc("__file_tailfile", file.TailFile(se))

	//FTP操作函数
	se.AddFunc("__file_newftp", NewFtp(se))
	se.AddFunc("__file_newsftp", NewSftp(se))

	//数据库操作函数
	se.AddFunc("__db_newdb", NewDB(se))
	se.AddFunc("__db_opendb", OpenDB(se))

	//配置文件操作函数
	se.AddFunc("__yaml_read", yaml.ReadFile(se))
	return se
}
