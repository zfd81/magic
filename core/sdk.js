(function () {
    String.prototype.left = function (len) {
        return __string_left(this, len)
    };
    String.prototype.right = function (len) {
        return __string_right(this, len)
    };
    String.prototype.lpad = function (len, padstr) {
        return __string_lpad(this, len, padstr)
    };
    String.prototype.rpad = function (len, padstr) {
        return __string_rpad(this, len, padstr)
    };
    String.prototype.splitn = function (sep, n) {
        if (sep == undefined) {
            sep = ",";
        }
        return __string_splitn(this, sep, n)
    };
    String.prototype.splitms = function () {
        return __string_splitms(this)
    };
}());

var $ = {
    args: __magic_args,
    root: function () {
        return __magic_root;
    },
    magic: function (name, ...args) {
        var err = __sys_execute(__magic_root, name, ...args);
        if (err != null) {
            throw err
        }
    },
    resp: {
        status: function (resp) {
            return resp["StatusCode"];
        },
        content: function (resp) {
            return resp["Content"];
        },
        data: function (resp) {
            var body = this.content(resp)
            try {
                return JSON.parse(body);
            } catch (e) {
                return {}
            }
        },
        header: function (resp) {
            return resp["Header"];
        }
    },
    HttpPromise: {
        create: function (response) {
            var promise = {
                status: $.resp.status(response) == 200,
                then: function (func) {
                    if (this.status) {
                        func($.resp.data(response), $.resp.header(response));
                    }
                    return this
                },
                catch: function (func) {
                    if (!this.status) {
                        func($.resp.status(response), $.resp.data(response), $.resp.header(response));
                    }
                }
            }
            return promise
        }
    },
    hget: function (url, param, header) {
        var resp = __http_get(url, param, header);
        var promise = this.HttpPromise.create(resp);
        return promise;
    },
    hpost: function (url, param, header) {
        var resp = __http_post(url, param, header);
        var promise = this.HttpPromise.create(resp);
        return promise;
    },
    hdelete: function (url, param, header) {
        var resp = __http_delete(url, param, header);
        var promise = this.HttpPromise.create(resp);
        return promise;
    },
    hput: function (url, param, header) {
        var resp = __http_put(url, param, header);
        var promise = this.HttpPromise.create(resp);
        return promise;
    },
    resp: {
        status: function (resp) {
            return resp["StatusCode"];
        },
        content: function (resp) {
            return resp["Content"];
        },
        data: function (resp) {
            var body = this.content(resp)
            try {
                return JSON.parse(body);
            } catch (e) {
                return {}
            }
        },
        header: function (resp) {
            return resp["Header"];
        },
        write: function (data, header) {
            __resp_write(data, header)
        },
        error: function (statusCode, data) {
            __resp_error(statusCode, data)
        }
    },
    File: {
        create: function (f) {
            var file = {
                exist: function () {
                    return f.IsExist();
                },
                name: function () {
                    return f.Name();
                },
                size: function () {
                    return f.Size();
                },
                modtime: function () {
                    return new Date(f.ModTime()*1000);
                },
                isdir: function () {
                    return f.IsDir();
                },
                rename: function (newname) {
                    f.Rename(newname)
                },
                remove: function () {
                    f.Remove()
                },
                mkdir: function () {
                    f.Mkdir()
                },
                copyTo: function (dest, override) {
                    if (override == undefined) {
                        override = true
                    }
                    return f.CopyTo(dest, override)
                },
                ls: function (reg) {
                    if (reg == undefined || reg == null) {
                        reg = "^[\\w\\W]*$";
                    } else {
                        reg = "^" + reg.replaceAll("*", "[\\w\\W]*") + "$"
                    }
                    var files = f.Ls();
                    return files.filter(item => {
                        return __string_match(item.name, reg)
                    })
                },
                lsFile: function (reg) {
                    return this.ls(reg).filter(item => {
                        return item.isdir == false
                    })
                },
                lsDir: function (reg) {
                    return this.ls(reg).filter(function (val) {
                        return val.isdir == true
                    })
                },
                readAll: function () {
                    return f.ReadAll()
                },
                readByLine: function (handler) {
                    f.ReadByLine(handler)
                },
                GetWriter:function (){
                    var w = f.GetWriter();
                    return {
                        close: function () {
                            w.Close()
                        },
                        writeStr: function (str) {
                            return w.WriteString(str)
                        },
                        writeLine: function (str) {
                            return w.WriteString(str + "\n")
                        },
                    }
                },
            }
            return file
        }
    },
    file: function (path) {
        var f = __file_newfile(path)
        return this.File.create(f)
    },
    ftail: function (path, handler, mustExist) {
        if (mustExist == undefined || mustExist == null) {
            mustExist = false
        }
        __file_tailfile(name, handler, mustExist)
    },
    Ftp: {
        create: function (client) {
            var ftp = {
                ls: function (rpath, reg) {
                    if (reg == undefined || reg == null) {
                        reg = "^[\\w\\W]*$";
                    } else {
                        reg = "^" + reg.replaceAll("*", "[\\w\\W]*") + "$"
                    }
                    var files = client.LsFunc(rpath)
                    return files.filter(item => {
                        return __string_match(item.name, reg)
                    })
                },
                lsfile: function (rpath, reg) {
                    return this.ls(rpath, reg).filter(item => {
                        return item.isdir == false
                    })
                },
                lsdir: function (rpath, reg) {
                    return this.ls(rpath, reg).filter(function (val) {
                        return val.isdir == true
                    })
                },
                mkdir: function (rpath) {
                    var err = client.Mkdir(rpath)
                    if (err != null) {
                        throw err
                    }
                },
                rmdir: function (rpath) {
                    var err = client.Rmdir(rpath)
                    if (err != null) {
                        throw err
                    }
                },
                get: function (rfile, lfile) {
                    var err = client.Get(rfile, lfile)
                    if (err != null) {
                        throw err
                    }
                },
                put: function (lfile, rfile) {
                    var err = client.Put(lfile, rfile)
                    if (err != null) {
                        throw err
                    }
                },
                delete: function (rfile) {
                    var err = client.Delete(rfile)
                    if (err != null) {
                        throw err
                    }
                },
                rename: function (rfile, newname) {
                    var err = client.Rename(rfile, newname)
                    if (err != null) {
                        throw err
                    }
                },
            }
            return ftp;
        }
    },
    newftp: function (addr, user, password) {
        var client = __file_newftp(addr, user, password)
        return this.Ftp.create(client)
    },
    Sftp: {
        create: function (client) {
            var sftp = {
                ls: function (rpath, reg) {
                    if (reg == undefined || reg == null) {
                        reg = "^[\\w\\W]*$";
                    } else {
                        reg = "^" + reg.replaceAll("*", "[\\w\\W]*") + "$"
                    }
                    var files = client.LsFunc(rpath)
                    return files.filter(item => {
                        return __string_match(item.name, reg)
                    })
                },
                lsfile: function (rpath, reg) {
                    return this.ls(rpath, reg).filter(item => {
                        return item.isdir == false
                    })
                },
                lsdir: function (rpath, reg) {
                    return this.ls(rpath, reg).filter(function (val) {
                        return val.isdir == true
                    })
                },
                mkdir: function (rpath) {
                    var err = client.Mkdir(rpath)
                    if (err != null) {
                        throw err
                    }
                },
                rmdir: function (rpath) {
                    var err = client.Rmdir(rpath)
                    if (err != null) {
                        throw err
                    }
                },
                get: function (rfile, lfile) {
                    var err = client.Get(rfile, lfile)
                    if (err != null) {
                        throw err
                    }
                },
                put: function (lfile, rfile) {
                    var err = client.Put(lfile, rfile)
                    if (err != null) {
                        throw err
                    }
                },
                delete: function (rfile) {
                    var err = client.Delete(rfile)
                    if (err != null) {
                        throw err
                    }
                },
                rename: function (rfile, newname) {
                    var err = client.Rename(rfile, newname)
                    if (err != null) {
                        throw err
                    }
                },
            }
            return sftp;
        }
    },
    newsftp: function (addr, user, password) {
        var client = __file_newsftp(addr, user, password)
        return this.Sftp.create(client)
    },
    DB: {
        Result: {
            create: function () {
                var result = {
                    status: false,
                    data: undefined,
                    msg: "",
                    setVal: function (val) {
                        this.data = val;
                        this.status = true;
                    },
                    setMsg: function (msg) {
                        this.msg = msg;
                    }
                }
                return result
            }
        },
        DBPromise: {
            create: function (result) {
                var promise = {
                    then: function (func) {
                        if (result.status) {
                            func(result.data);
                        }
                        return this
                    },
                    catch: function (func) {
                        if (!result.status) {
                            func(result.msg);
                        }
                    }
                }
                return promise
            }
        },
        create: function (client) {
            var db = {
                query: function (sql, arg, pageNumber, pageSize) {
                    var result = $.DB.Result.create();
                    try {
                        if (sql == undefined || sql == null || sql == "") {
                            result.setMsg("SQL statement cannot be empty")
                        } else {
                            if (arg == undefined) {
                                arg = null;
                            }
                            if (pageNumber == undefined) {
                                pageNumber = 0;
                            } else {
                                if (typeof pageNumber != "number") {
                                    pageNumber = parseInt(pageNumber)
                                }
                            }
                            if (pageSize == undefined) {
                                pageSize = 20;
                            } else {
                                if (typeof pageSize != "number") {
                                    pageSize = parseInt(pageSize)
                                }
                            }
                            result.setVal(client.QueryFunc(sql, arg, pageNumber, pageSize));
                        }
                    } catch (err) {
                        result.setMsg(err);
                    }
                    return $.DB.DBPromise.create(result);
                },
                queryOne: function (sql, arg) {
                    var result = $.DB.Result.create();
                    try {
                        if (sql == undefined || sql == null || sql == "") {
                            result.setMsg("SQL statement cannot be empty")
                        } else {
                            if (arg == undefined) {
                                arg = null;
                            }
                            result.setVal(client.QueryOneFunc(sql, arg));
                        }
                    } catch (err) {
                        result.setMsg(err);
                    }
                    return $.DB.DBPromise.create(result);
                },
                save: function (table, arg) {
                    var result = $.DB.Result.create();
                    try {
                        if (table == undefined || table == null || table == "") {
                            result.setMsg("Table name cannot be empty")
                        } else if (arg == undefined || typeof arg != "object") {
                            result.setMsg("Parameter data type error")
                        } else {
                            result.setVal(client.SaveFunc(table, arg));
                        }
                    } catch (err) {
                        result.setMsg(err);
                    }
                    return $.DB.DBPromise.create(result);
                },
                exec: function (sql, arg) {
                    var result = $.DB.Result.create();
                    try {
                        if (sql == undefined || sql == null || sql == "") {
                            result.setMsg("SQL statement cannot be empty")
                        } else {
                            if (arg == undefined) {
                                arg = {};
                            }
                            result.setVal(client.ExecFunc(sql, arg));
                        }
                    } catch (err) {
                        result.setMsg(err);
                    }
                    return $.DB.DBPromise.create(result);
                },
                writeFile: function (sql, arg, name, sep) {
                    if (sql == undefined || sql == null || sql == "") {
                        throw "SQL statement cannot be empty"
                    }
                    if (name == undefined || name == null || name == "") {
                        throw "The file name cannot be empty"
                    }
                    if (arg == undefined) {
                        arg = {};
                    }
                    if (sep == undefined) {
                        sep = ",";
                    }
                    return client.WriteFileFunc(sql, arg, name, sep);
                },
            }
            return db
        },
        newDB: function (driver, host, port, user, password, dbName) {
            var client = __db_newdb(driver, host, port, user, password, dbName)
            return this.create(client)
        },
    },
    openDB: function (name) {
        var client = __db_opendb(name)
        if (client == null) {
            return null
        }
        return this.DB.create(client)
    },
    mysql: function (host, port, user, password, dbName) {
        return this.DB.newDB("mysql", host, port, user, password, dbName)
    },
    mssql: function (host, port, user, password, dbName) {
        return this.DB.newDB("mssql", host, port, user, password, dbName)
    },
    pgsql: function (host, port, user, password, dbName) {
        return this.DB.newDB("postgresql", host, port, user, password, dbName)
    },
    yaml: {
        read: function (path) {
            return __yaml_read(path)
        }
    },
};