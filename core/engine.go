package core

import (
	"bytes"
	"fmt"
	"gitee.com/zfd81/magic/httpclient"
	"gitee.com/zfd81/rooster/rsql"
	"path/filepath"
	"reflect"
	"strings"

	js "github.com/dop251/goja"
)

func convertValue(val js.Value) (interface{}, error) {
	switch val.ExportType().Kind() {
	case reflect.String:
		return val.ToString(), nil
	case reflect.Map:
		return val.Export(), nil
	case reflect.Struct:
		return val.Export(), nil
	case reflect.Int, reflect.Int64:
		return val.ToInteger(), nil
	case reflect.Float64:
		return val.ToFloat(), nil
	case reflect.Bool:
		return val.ToBoolean(), nil
	default:
		return nil, fmt.Errorf("no matching data type found")
	}
}

type Context interface {
	PID() string
	SelectDataSource(name string) *rsql.DB
	Response() *httpclient.Response
	Get(attribute string) interface{}
	Set(attribute string, value interface{})
}

type Engine struct {
	root      string
	vm        *js.Runtime
	sdk       string
	libraries *bytes.Buffer
	script    *bytes.Buffer
	ctx       Context
	modules   map[string]js.Value
}

func (e *Engine) SetRootPath(path string) {
	e.root = path
}

func (e *Engine) RootPath() string {
	return e.root
}

func (e *Engine) AbsPath(path string) string {
	path = strings.TrimSpace(path)
	path = filepath.Clean(path)
	if filepath.IsAbs(path) {
		return path
	}
	return filepath.Join(e.root, path)
}

func (e *Engine) GetRuntime() *js.Runtime {
	return e.vm
}

func (e *Engine) ThrowException(format string, msgs ...interface{}) {
	panic(e.vm.ToValue(fmt.Sprintf(format, msgs...)))
}

func (e *Engine) GetSDK() string {
	return e.sdk
}

func (e *Engine) GetLibraries() string {
	return e.libraries.String()
}

func (e *Engine) SetVar(name string, value interface{}) error {
	return e.vm.Set(name, value)
}

func (e *Engine) GetVar(name string) (interface{}, error) {
	value := e.vm.Get(name)
	if value == nil {
		return nil, fmt.Errorf("variable '%s' not found", name)
	}
	return convertValue(value)
}

func (e *Engine) GetMlVar(name string) (interface{}, error) {
	names := strings.Split(name, ".")
	value := e.vm.Get(names[0])
	if value == nil {
		return nil, fmt.Errorf("variable '%s' not found", name)
	}
	for i := 1; i < len(names); i++ {
		if value.ExportType().Kind() != reflect.Map {
			return nil, fmt.Errorf("variable '%s' not found", name)
		}
		value = value.ToObject(e.vm).Get(names[i])
		if value == nil {
			return nil, fmt.Errorf("variable '%s' not found", name)
		}
	}
	return convertValue(value)
}

func (e *Engine) AddFunc(name string, function interface{}) error {
	return e.vm.Set(name, function)
}

func (e *Engine) GetFunc(name string) (Function, error) {
	f, ok := js.AssertFunction(e.vm.Get(name))
	if !ok {
		return nil, fmt.Errorf("function '%s' not found", name)
	}
	return &MagicFunction{
		vm:       e.vm,
		name:     name,
		function: f,
	}, nil
}

func (e *Engine) GetMlFunc(name string) (Function, error) {
	names := strings.Split(name, ".")
	length := len(names)
	if length < 2 {
		return e.GetFunc(name)
	} else {
		value := e.vm.Get(names[0])
		if value == nil {
			return nil, fmt.Errorf("function '%s' not found", name)
		}
		for i := 1; i < length; i++ {
			if value.ExportType().Kind() != reflect.Map {
				return nil, fmt.Errorf("function '%s' not found", name)
			}
			value = value.ToObject(e.vm).Get(names[i])
			if value == nil {
				return nil, fmt.Errorf("function '%s' not found", name)
			}
		}
		f, ok := js.AssertFunction(value)
		if !ok {
			return nil, fmt.Errorf("function '%s' not found", name)
		}
		return &MagicFunction{
			vm:       e.vm,
			name:     name,
			function: f,
		}, nil
	}
}

func (e *Engine) CallFunc(name string, args ...interface{}) (interface{}, error) {
	f, err := e.GetMlFunc(name)
	if err != nil {
		return nil, err
	}
	return f.Perform(args...)
}

func (e *Engine) AddLibrary(src string) {
	e.libraries.WriteString(src)
}

func (e *Engine) SetScript(src string) {
	e.script.Reset()
	e.script.WriteString(src)
}

func (e *Engine) AddScript(src string) {
	e.script.WriteString(src)
}

func (e *Engine) AddModule(path string, module js.Value) {
	_, found := e.modules[path]
	if found {
		return
	}
	e.modules[path] = module
}

func (e *Engine) GetModule(path string) js.Value {
	m, found := e.modules[path]
	if found {
		return m
	}
	return nil
}

func (e *Engine) Run(ctx Context) error {
	e.ctx = ctx
	_, err := e.vm.RunString(e.sdk + e.libraries.String() + e.script.String())
	return err
}

func (e *Engine) Reset() {
	e = &Engine{
		vm:        js.New(),
		sdk:       sdkSource,
		libraries: e.libraries,
		script:    bytes.NewBufferString(""),
		modules:   map[string]js.Value{},
	}
}

func (e *Engine) Context() Context {
	return e.ctx
}

func NewEngine() *Engine {
	se := &Engine{
		vm:        js.New(),
		sdk:       sdkSource,
		libraries: bytes.NewBufferString(""),
		script:    bytes.NewBufferString(""),
		modules:   map[string]js.Value{},
	}
	return InitializeSystem(se)
}
