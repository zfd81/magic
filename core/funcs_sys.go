package core

import (
	"fmt"
	"os"
	"path/filepath"
	"time"
)

func Scan(e *Engine) func(prompt string) string {
	return func(prompt string) string {
		fmt.Print(prompt)
		var input string
		_, err := fmt.Scan(&input)
		if err != nil {
			e.ThrowException(err.Error())
		}
		return input
	}
}

func Print(e *Engine) func(...interface{}) {
	return func(args ...interface{}) {
		_, err := fmt.Print(args...)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func Println(e *Engine) func(...interface{}) {
	return func(args ...interface{}) {
		_, err := fmt.Println(args...)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func Printf(e *Engine) func(string, ...interface{}) {
	return func(format string, args ...interface{}) {
		_, err := fmt.Printf(format, args...)
		if err != nil {
			e.ThrowException(err.Error())
		}
	}
}

func Sleep(sec time.Duration) {
	time.Sleep(time.Second * sec)
}

func Execute(dir, name string, args ...interface{}) error {
	path := filepath.Join(dir, name)
	info, err := os.Stat(path)
	if err != nil || info.IsDir() {
		return fmt.Errorf("stat %s: no such file", name)
	}

	bytes, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("read file %s failed: %v", name, err)
	}

	go func() {
		js := NewEngine()
		js.SetVar("__magic_root", dir)
		js.SetVar("__magic_args", args)
		js.SetScript(string(bytes))
		err = js.Run(nil)
		if err != nil {
			return
		}
	}()
	return err
}
