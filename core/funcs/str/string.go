package str

import (
	"bytes"
	"gitee.com/zfd81/magic/engine"
	"regexp"
	"strings"

	u "gitee.com/zfd81/rooster/util"
	"github.com/spf13/cast"
)

func Left(str string, length int) string {
	return u.Left(str, length)
}

func Right(str string, length int) string {
	return u.Right(str, length)
}

func Lpad(str string, length int, padstr string) string {
	if str == "" || length < 0 {
		return str
	}
	strRune := []rune(str)
	var len = length - len(strRune)
	if len > 0 {
		for i := 0; i < len; i++ {
			str = padstr + str
		}
		return str
	} else if len == 0 {
		return str
	} else {
		return string(strRune[0:length])
	}
}

func Rpad(str string, length int, padstr string) string {
	if str == "" || length < 0 {
		return str
	}
	strRune := []rune(str)
	var len = length - len(strRune)
	if len > 0 {
		for i := 0; i < len; i++ {
			str = str + padstr
		}
		return str
	} else if len == 0 {
		return str
	} else {
		return string(strRune[0:length])
	}
}

func StartsWith(str, prefix string) bool {
	return strings.HasPrefix(str, prefix)
}

func EndsWith(str, suffix string) bool {
	return strings.HasSuffix(str, suffix)
}

func Split(str, sep string) []string {
	return strings.Split(str, sep)
}

func SplitN(str, sep string, n int) []string {
	return strings.SplitN(str, sep, n)
}

// 多空格拆分
func SplitMS(str string) []string {
	r := regexp.MustCompile("[^\\s]+")
	return r.FindAllString(str, -1)
}

func ReplaceAll(str, old, new string) string {
	return strings.ReplaceAll(str, old, new)
}

func Join(m engine.Magic) func(interface{}, string) string {
	return func(arg interface{}, sep string) string {
		elems, ok := arg.([]interface{})
		if !ok {
			m.ThrowException("function join(array, sep) error: please enter the array type parameter.")
		}
		if sep == "" {
			sep = " "
		}
		length := len(elems)
		switch length {
		case 0:
			return ""
		case 1:
			return cast.ToString(elems[0])
		default:
			var str bytes.Buffer
			str.WriteString(cast.ToString(elems[0]))
			for i := 1; i < length; i++ {
				str.WriteString(sep)
				str.WriteString(cast.ToString(elems[i]))
			}
			return str.String()
		}
	}
}

func MatchString(m engine.Magic) func(string, string) bool {
	return func(str, reg string) bool {
		matched, err := regexp.MatchString(reg, str)
		if err != nil {
			m.ThrowException(err.Error())
		}
		return matched
	}
}
