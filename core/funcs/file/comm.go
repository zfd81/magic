package file

import (
	"bufio"
	"io"
	"os"
	"path/filepath"
)

func IsExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		return os.IsExist(err)
	}
	return true
}

func Remove(path string) error {
	return os.RemoveAll(path)
}

func CreateFile(path string) (*os.File, error) {
	dir, _ := filepath.Split(path)
	if !IsExist(dir) {
		os.MkdirAll(dir, os.ModePerm)
	}
	return os.Create(path)
}

func FileCopy(src, dest string) (int64, error) {
	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := CreateFile(dest)
	if err != nil {
		return 0, err
	}
	defer destination.Close()

	size := 1024 * 128
	buf := make([]byte, size)
	return io.CopyBuffer(destination, source, buf)
}

func DirCopy(src, dest string) (int64, error) {
	var size int64
	items, err := os.ReadDir(src)
	if err != nil {
		return size, nil
	}
	for _, item := range items {
		if !item.IsDir() {
			nw, err := FileCopy(filepath.Join(src, item.Name()), filepath.Join(dest, item.Name()))
			if err != nil {
				return size, err
			}
			size += nw
			continue
		}

		err = os.MkdirAll(filepath.Join(dest, item.Name()), os.ModePerm)
		if err != nil {
			return size, err
		}

		nw, err := DirCopy(filepath.Join(src, item.Name()), filepath.Join(dest, item.Name()))
		if err != nil {
			return size, err
		}

		size += nw
	}
	return size, nil
}

func ReadFileByLine(path string, handler func(line string) bool) (err error) {
	ok := true
	defer func() {
		if r := recover(); r != nil {
			ok = false
			switch r := r.(type) {
			case error:
				err = r
			default:
				panic(r)
			}
		}
	}()

	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	dataStream := make(chan string, 2000)
	scanner := bufio.NewScanner(file)

	go func() {
		defer close(dataStream)
		for scanner.Scan() {
			if !ok {
				break
			}
			dataStream <- scanner.Text()
		}
	}()

	for row := range dataStream {
		if !handler(row) {
			return nil
		}
	}
	return
}
