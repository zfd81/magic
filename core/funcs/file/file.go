package file

import (
	"bufio"
	"gitee.com/zfd81/magic/engine"
	"github.com/hpcloud/tail"
	"github.com/spf13/cast"
	"io/fs"
	"os"
	"path/filepath"
	"time"
)

type FileWriter struct {
	m      engine.Magic
	file   *os.File
	writer *bufio.Writer
}

func (w *FileWriter) Close() {
	defer w.file.Close()
	w.writer.Flush()
}

func (w *FileWriter) WriteString(s string) int {
	cnt, err := w.writer.WriteString(s)
	if err != nil {
		w.Close()
		w.m.ThrowException("write string error :%v", err)
	}
	return cnt
}

func (w *FileWriter) Write(data interface{}) int {
	var cnt int
	var err error
	switch val := data.(type) {
	case []byte:
		cnt, err = w.writer.Write(val)
	case string:
		cnt, err = w.writer.WriteString(val)
	default:
		cnt, err = w.writer.WriteString(cast.ToString(val))
	}
	if err != nil {
		w.Close()
		w.m.ThrowException("write error :%v", err)
	}
	return cnt
}

type File struct {
	m       engine.Magic
	exist   bool
	path    string
	abspath string
	info    os.FileInfo
}

func (f *File) checkExists() {
	if !f.exist {
		f.m.ThrowException("file %s not exist", f.path)
	}
}

func (f *File) IsExist() bool {
	return f.exist
}

func (f *File) Name() string {
	_, name := filepath.Split(f.abspath)
	return name
}

func (f *File) Size() int64 {
	f.checkExists()
	return f.info.Size()
}

func (f *File) Mode() fs.FileMode {
	f.checkExists()
	return f.info.Mode()
}

func (f *File) ModTime() int64 {
	f.checkExists()
	return f.info.ModTime().Unix()
}

func (f *File) IsDir() bool {
	f.checkExists()
	return f.info.IsDir()
}

func (f *File) Rename(newname string) {
	f.checkExists()
	d, _ := filepath.Split(f.abspath)
	if err := os.Rename(f.abspath, filepath.Join(d, newname)); err != nil {
		f.m.ThrowException("rename file %s  error: %v", f.path, err)
	}
}

func (f *File) Remove() {
	if !f.exist {
		return
	}

	if err := os.RemoveAll(f.abspath); err != nil {
		f.m.ThrowException("remove file %s error: %v", f.path, err)
	}
}

func (f *File) Mkdir() {
	if f.exist {
		f.m.ThrowException("file %s already exists", f.path)
	}
	if err := os.MkdirAll(f.abspath, os.ModePerm); err != nil {
		f.m.ThrowException("create directory %s error: %v", f.path, err)
	}
}

func (f *File) CopyTo(dest string, override bool) int64 {
	f.checkExists()
	abs := f.m.AbsPath(dest)
	if IsExist(abs) {
		if override {
			if err := Remove(abs); err != nil {
				f.m.ThrowException("remove file %s error: %v", dest, err)
			}
		} else {
			f.m.ThrowException("file %s already exists", dest)
		}
	}

	if f.IsDir() {
		size, err := DirCopy(f.abspath, abs)
		if err != nil {
			f.m.ThrowException("copy directory %s error: %v", f.path, err)
		}
		return size
	} else {
		if !f.Mode().IsRegular() {
			f.m.ThrowException("%s is not a regular file", f.path)
		}
		size, err := FileCopy(f.abspath, abs)
		if err != nil {
			f.m.ThrowException("copy file %s error: %v", f.path, err)
		}
		return size
	}
}

func (f *File) Ls() []map[string]interface{} {
	f.checkExists()
	if !f.IsDir() {
		f.m.ThrowException("ls error: %s is a file", f.path)
	}

	entrys, err := os.ReadDir(f.abspath)
	if err != nil {
		f.m.ThrowException("ls error: %v", err)
	}

	items := []map[string]interface{}{}
	for _, entry := range entrys {
		items = append(items, map[string]interface{}{
			"name":  entry.Name(),
			"path":  filepath.Join(f.abspath, entry.Name()),
			"isdir": entry.IsDir(),
		})
	}
	return items
}

func (f *File) ReadAll() string {
	f.checkExists()
	if f.IsDir() {
		f.m.ThrowException("open %s: No such file", f.path)
	}
	bytes, err := os.ReadFile(f.abspath)
	if err != nil {
		f.m.ThrowException(err.Error())
	}
	return string(bytes)
}

func (f *File) ReadByLine(handler func(line string) bool) {
	f.checkExists()
	if f.IsDir() {
		f.m.ThrowException("open %s: No such file", f.path)
	}

	err := ReadFileByLine(f.abspath, handler)
	if err != nil {
		f.m.ThrowException("read file %s failed: %v", f.path, err)
	}
}

func (f *File) GetWriter() *FileWriter {
	file, err := CreateFile(f.abspath)
	if err != nil {
		f.m.ThrowException("open file error: %v", err)
	}
	return &FileWriter{
		file:   file,
		writer: bufio.NewWriterSize(file, 1024*10),
	}
}

func NewFile(m engine.Magic) func(string) *File {
	return func(path string) *File {
		exist := true
		abs := m.AbsPath(path)

		info, err := os.Stat(abs)
		if err != nil {
			if os.IsNotExist(err) {
				exist = false
			} else {
				m.ThrowException("open %s error: %v", path, err)
			}
		}

		return &File{
			m:       m,
			exist:   exist,
			path:    path,
			abspath: abs,
			info:    info,
		}
	}
}

func ReadFile(m engine.Magic) func(string) string {
	return func(path string) string {
		abs := m.AbsPath(path)
		stat, err := os.Stat(abs)
		if err != nil || stat.IsDir() {
			m.ThrowException("open %s: No such file", path)
		}

		bytes, err := os.ReadFile(abs)
		if err != nil {
			m.ThrowException(err.Error())
		}
		return string(bytes)
	}
}

func TailFile(m engine.Magic) func(string, func(line string) bool, bool) {
	return func(path string, handler func(line string) bool, mustExist bool) {
		config := tail.Config{
			ReOpen:    true,                                 // 重新打开
			Follow:    true,                                 // 是否跟随
			Location:  &tail.SeekInfo{Offset: 0, Whence: 2}, // 从文件的哪个地方开始读
			MustExist: mustExist,                            // 文件不存在不报错
			Poll:      true,
			Logger:    tail.DiscardingLogger,
		}
		tails, err := tail.TailFile(m.AbsPath(path), config)
		if err != nil {
			m.ThrowException("tail file %s error: %v", path, err)
		}
		var line *tail.Line
		var ok bool
		for {
			line, ok = <-tails.Lines //遍历chan，读取日志内容
			if !ok {
				time.Sleep(time.Second)
				continue
			}
			if !handler(line.Text) {
				break
			}
		}
	}
}
