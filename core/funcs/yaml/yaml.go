package yaml

import (
	"gitee.com/zfd81/magic/engine"
	"gopkg.in/yaml.v3"
	"os"
)

func ReadFile(m engine.Magic) func(string) map[string]interface{} {
	return func(path string) map[string]interface{} {
		abs := m.AbsPath(path)

		info, err := os.Stat(abs)
		if err != nil || info.IsDir() {
			m.ThrowException("open %s: No such file", path)
		}

		bytes, err := os.ReadFile(abs)
		if err != nil {
			m.ThrowException("Read file %s failed: %v", path, err)
		}

		var result map[string]interface{}
		err = yaml.Unmarshal(bytes, &result)
		if err != nil {
			m.ThrowException("Fatal error when reading %s conf, unable to decode into struct, %v", path, err)
		}

		return result
	}
}
