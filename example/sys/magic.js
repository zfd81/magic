/**
 * root()
 * 主程序的根目录
 */
println($.root())

/**
 * sleep(sec)
 * 休眠函数，单位（秒）
 */
println("Start Sleep")
sleep(1);//休眠5秒
println("Sleep End")


/**
 * magic(name)
 * 启动新线程执行程序
 */
$.magic("subprogram.js",1,"aa","bb")
sleep(1);
println("主程序结束");
println("主线程根目录为：",$.root())