/**
 * print(...args) 标准输出
 * 内容之间不存在空格
 * 不换行
 */
print("January");
print(2, "February", 3, "March\n");

/**
 * println(...args) 标准输出
 * 内容之间存在空格
 * 换行
 */
println("April");
println(5, "May", 6, "June");

/**
 * printf(...args) 格式化输出
 * 
 * %s           字符串
 * %t           true 或 false
 * %b           二进制表示 
 * %c           相应Unicode码点所表示的字符 
 * %d           十进制表示 
 * %o           八进制表示 
 * %x           十六进制表示，字母形式为小写 a-f 
 * %X           十六进制表示，字母形式为大写 A-F 
 * %U           Unicode格式：123，等同于 "U+007B
 * %f, %g, %e   浮点数
 * %v           原值输出
 */
printf("I like %s.", "July") //格式化输出,不换行
printf("I'm %d years old. \n", 33) //格式化输出,换行\n
printf("I'm %s, %d years old. \n", "zfd", 42)

