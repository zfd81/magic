const Big = require("lib/big.min.js")

/**
 * 常量定义
 *
 * big.js的常量定义一共有5个，分别的含义是：
 *
 * DP，小数点后位数，默认值是20
 *
 * RM，四舍五入方式，默认为1，代表向最近的整数取整。如果是0.5，那么向下取整。
 *
 * NE：在转换为字符串时展示为科学计数法的最小小数位数。默认值是-7，即小数点后第7为才开始不是0。
 *
 * PE：在转换为字符串时展示位科学计数法的最小整数位数。默认值是21，即数字长度超过21位。
 *
 * strict：默认值为false。设置为true时，构造函数只接受字符串和大数。
 */

/**
 * 运算符操作函数
 * abs，取绝对值。
 *
 * cmp，compare的缩写，即比较函数。
 *
 * div，除法。
 *
 * eq，equal的缩写，即相等比较。
 *
 * gt，大于。
 *
 * gte，小于等于，e表示equal。
 *
 * lt，小于。
 *
 * lte，小于等于，e表示equal。
 *
 * minus，减法。
 *
 * mod，取余。
 *
 * plus，加法。
 *
 * pow，次方。
 *
 * prec，按精度舍入，参数表示整体位数。
 *
 * round，按精度舍入，参数表示小数点后位数。
 *
 * sqrt，开方。
 *
 * times，乘法。
 *
 * toExponential，转化为科学计数法，参数代表精度位数。
 *
 * toFied，补全位数，参数代表小数点后位数。
 *
 * toJSON和toString，转化为字符串。
 *
 * toPrecision，按指定有效位数展示，参数为有效位数。
 *
 * toNumber，转化为JavaScript中number类型。
 *
 * valueOf，包含负号（如果为负数或者-0）的字符串。
 */


//Big实例的构造函数。它接受number、string或Big number对象类型的值。
x = new Big(123.4567)
y = Big('123456.7e-3')             // 'new' is optional
z = new Big(x)
flag=x.eq(y) && x.eq(z) && y.eq(z)      // true
println(flag)

//一个Big数字是不可变的，因为它不会被它的方法改变。
x = new Big(0.3)
x.minus(0.1)                       // "0.2"
println(x)


//Big返回的方法可以被连接
n=x.div(y).plus(z).times(9).minus('1.234567801234567e+8').plus(976.54321).div('2598.11772')
x.sqrt().div(y).pow(3).gt(y.mod(z))
println(n)

//与JavaScript的数字类型一样，还有toExponential、toFixed和toPrecision方法
x = new Big(255.5)
println(x.toExponential(5))                 // "2.55500e+2"
println(x.toFixed(5))                   // "255.50000"
println(x.toPrecision(5))                // "255.50"

//算术方法总是返回精确的结果，除了div、sqrt和pow（带负指数），因为这些方法涉及除法。
//用于对这些方法的结果进行舍入的最大小数位数和舍入模式是由Big数字构造函数的DP和RM属性的值决定的。
Big.DP = 10
Big.RM = 1

x = new Big(2);
y = new Big(3);
z = x.div(y)                       // "0.6666666667"
println(z.sqrt())                           // "0.8164965809"
println(z.pow(-3))                        // "3.3749999995"
println(z.times(z))                        // "0.44444444448888888889"
println(z.times(z).round(10))            // "0.4444444445"

