var _ = require("lib/lodash.min.js")

var users = [
    {'user': 'barney', 'age': 36, 'active': true},
    {'user': 'fred', 'age': 40, 'active': false}
];

var vals = _.filter(users, function (o) {
    return !o.active;
})
println(vals[0].user)

