/**
 * newsftp(addr, user, password)
 * 创建新的Sftp连接
 */
var sftp = $.newsftp("172.19.90.151:21", "zfd", "123456")

/**
 * ls(rpath, reg)
 * 查看远程目录下的文件和子目录
 */
try {
    files = sftp.ls("/root")
    files.forEach(value => {
        $.println(value.name, ":", value.isdir, ":", value.path)
    });
    $.println("--------------------------------------------------")
} catch (error) {
    $.println("err:", error)
}

/**
 * lsfile(rpath, reg)
 * 查看远程目录下的文件
 */
try {
    files = sftp.lsfile("/root")
    files.forEach(value => {
        $.println(value.name, ":", value.isdir,":",value.path)
    });
    $.println("--------------------------------------------------")
} catch (error) {
    $.println("err:", error)
}

/**
 * lsdir(rpath, reg)
 * 查看远程目录下的子目录
 */
try {
    files = sftp.lsdir("/root")
    files.forEach(value => {
        $.println(value.name, ":", value.isdir,":",value.path)
    });
    $.println("--------------------------------------------------")
} catch (error) {
    $.println("err:", error)
}

//查询以'.'开头的子目录
try {
    files = sftp.lsdir("/root", "\\.*")
    files.forEach(value => {
        $.println(value.name, ":", value.isdir,":",value.path)
    });
    $.println("--------------------------------------------------")
} catch (error) {
    $.println("err:", error)
}

/**
 * rmdir(rpath)
 * 删除远程目录
 */
try {
    sftp.rmdir("/root/aaa/ccc")
} catch (error) {
    $.println("rmdir err:", error)
}

/**
 * mkdir(rpath)
 * 创建远程目录
 */
try {
    sftp.mkdir("/root/aaa/bbb/")
} catch (error) {
    $.println("mkdir err:", error)
}

/**
 * get(rfile, lfile)
 * 获得远程文件
 */
sftp.get("/root/rock.out", "example/dir/rock.out1")

/**
 * put(lfile, rfile)
 * 上传本地文件
 */
try {
    sftp.put("example/dir/rock.out1", "/root/aaa/newfile.out")
} catch (error) {
    $.println("err:", error)
}

/**
 * rename(rfile, name)
 * 修改远程文件或目录的名字
 */
try {
    sftp.rename("/root/aaa/newfile.out","file_new.out")//修改文件名
} catch (error) {
    $.println("rename err:", error)
}

try {
    sftp.rename("/root/aaa/bbb","ccc")//修改目录名
} catch (error) {
    $.println("rename err:", error)
}

/**
 * delete(rpath)
 * 删除远程文件
 */
try {
    sftp.delete("/root/aaa/file_new.out")
} catch (error) {
    $.println("err:", error)
}