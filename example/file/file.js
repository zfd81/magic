try {
    var f = $.file("./file.js2")
} catch (err) {
    println(err);
}

println(f.exist()) //文件是否存在
println(f.name()) //文件名
println(f.size()) //文件大小
println(f.modtime()) //文件修改时间
println(f.isdir()) //文件是否是目录
f.rename("file.js2") //文件重命名

$.file("tmp").remove()//删除文件
$.file("tmp").mkdir()//创建目录

f.copyTo("tmp/file.js")

/**
 * ls([reg])
 * 列出目录下的文件和子目录
 */
var files = $.file("./").ls()
files.forEach((value) => {
    print("文件名:", value.name, " ,") //文件名
    print("是否是目录：", value.isdir, " ,") //是否是目录
    println("文件路径：", value.path) //文件路径
});
println("-----------------------------------------------------------------------------")

var files = $.file("./").ls("*js") //查询文件名以'js'结尾的文件和目录
files.forEach((value) => {
    print("文件名:", value.name, " ,") //文件名
    print("是否是目录：", value.isdir, " ,") //是否是目录
    println("文件路径：", value.path) //文件路径
});
println("-----------------------------------------------------------------------------")

/**
 * lsFile([reg])
 * 列出目录下的文件
 */
var files = $.file("./").lsFile()
files.forEach(value => {
    print("文件名:", value.name, " ,") //文件名
    print("是否是目录：", value.isdir, " ,") //是否是目录
    println("文件路径：", value.path) //文件路径
});
println("-----------------------------------------------------------------------------")

/**
 * lsDir([reg])
 * 列出目录下的子目录
 */
var files = $.file("./").lsDir()
files.forEach(value => {
    print("文件名:", value.name, " ,") //文件名
    print("是否是目录：", value.isdir, " ,") //是否是目录
    println("文件路径：", value.path) //文件路径
});