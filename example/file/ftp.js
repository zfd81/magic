/**
 * newftp(addr, user, password)
 * 创建新的Ftp连接
 */
var ftp = $.newftp("172.16.26.71:2231", "datacloud", "bonc!QAZ2wsx")

try {
    /**
     * ls(rpath, reg)
     * 查看远程目录下的文件和子目录
     */
    files = ftp.ls("/test")
    files.forEach(value => {
        print("文件名:", value.name, " ,") //文件名
        print("是否是目录：", value.isdir, " ,") //是否是目录
        println("文件路径：", value.path) //文件路径
    });
} catch (error) {
    println("err:", error)
}

println("--------------------------------------------------------------------------------")

try {
    /**
     * lsfile(rpath, reg)
     * 查看远程目录下的文件
     */
    files = ftp.lsfile("/")
    files.forEach(value => {
        print("文件名:", value.name, " ,") //文件名
        print("是否是目录：", value.isdir, " ,") //是否是目录
        println("文件路径：", value.path) //文件路径
    });
} catch (error) {
    println("err:", error)
}

println("--------------------------------------------------------------------------------")


try {
    /**
     * lsdir(rpath, reg)
     * 查看远程目录下的子目录
     */
    files = ftp.lsdir("/")
    files.forEach(value => {
        print("文件名:", value.name, " ,") //文件名
        print("是否是目录：", value.isdir, " ,") //是否是目录
        println("文件路径：", value.path) //文件路径
    });
} catch (error) {
    println("err:", error)
}

println("--------------------------------------------------------------------------------")

//查询以'V2.5'开头的子目录
try {
    files = ftp.lsdir("/", "V2.5*")
    files.forEach(value => {
        print("文件名:", value.name, " ,") //文件名
        print("是否是目录：", value.isdir, " ,") //是否是目录
        println("文件路径：", value.path) //文件路径
    });
} catch (error) {
    println("err:", error)
}

println("--------------------------------------------------------------------------------")

try {
    /**
     * rmdir(rpath)
     * 删除远程目录
     */
    ftp.rmdir("/ftpfile/aaa/aabbcc")
    println("删除目录成功")
} catch (error) {
    println("rmdir err:", error)
}

println("--------------------------------------------------------------------------------")

try {
    /**
     * mkdir(rpath)
     * 创建远程目录
     */
    ftp.mkdir("/ftpfile/aaa/aabbcc/")
    println("创建目录成功")
} catch (error) {
    println("mkdir err:", error)
}

println("--------------------------------------------------------------------------------")

try {
    /**
     * get(rfile, lfile)
     * 获得远程文件
     */
    ftp.get("webapps.tar.gz", "example/dir/webapps_new.tar.gz")
    println("获得文件成功")
} catch (error) {
    println("get err:", error)
}

println("--------------------------------------------------------------------------------")

try {
    /**
     * put(lfile, rfile)
     * 上传本地文件
     */
    ftp.put("README.md", "/ftpfile/aaa/bbb/read.md")
    println("上传文件成功")
} catch (error) {
    println("put err:", error)
}

println("--------------------------------------------------------------------------------")

try {
    /**
     * rename(rfile, name)
     * 修改远程文件或目录的名字
     */
    ftp.rename("/ftpfile/aaa/bbb/read.md", "read_new.md")//修改文件名
    println("修改文件名成功")

} catch (error) {
    println("rename file err:", error)
}

try {
    ftp.rename("/ftpfile/aaa/bbb", "aabbcc")//修改目录名
    println("修改目录名成功")
} catch (error) {
    println("rename dir err:", error)
}

println("--------------------------------------------------------------------------------")

try {
    files = ftp.ls("/ftpfile/aaa")
    files.forEach(value => {
        print("文件名:", value.name, " ,") //文件名
        print("是否是目录：", value.isdir, " ,") //是否是目录
        println("文件路径：", value.path) //文件路径
    });

    /**
     * delete(rpath)
     * 删除远程文件
     */
    ftp.delete("/ftpfile/aaa/aabbcc/read.md")
} catch (error) {
    println("delete err:", error)
}