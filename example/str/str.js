/**
 * String.splitn(str, sep, n)
 * String对象扩展方法
 * 通过分隔符sep把字符串分割成字符串数组,参数n决定返回的数组的数目
 *   n > 0: 返回的数组最多n个子字符串；最后一个子字符串包含未进行切割的部分
 *   n == 0: 返回空数组
 *   n < 0: 返回所有的子字符串组成的数组
 *   sep:分隔符，缺省值为逗号","
 */
println("ab,cd,ef".splitn( ",", 5)) // 返回["ab" "cd" "ef"]
println("ab,cd,ef".splitn( ",", 2)) // 返回["ab" "cd,ef"]
println("ab,cd,ef".splitn( ",", 0)) // 返回[]
println("ab,cd,ef".splitn( ",", -1)) // 返回["ab" "cd" "ef"]
println("ab,cd,ef".splitn( "", 5)) // 返回["a" "b" "," "c" "d,ef"]
println(",ab,cd,ef".splitn( ",", 5)) // 返回["" "ab" "cd" "ef"]
println("ab,cd,ef,".splitn(",", 2)) // 返回["ab" "cd,ef,"]
println("-----------------------------------------------------------------------------")

/**
 * String.splitms()
 * String对象扩展方法
 * 通过空格把字符串分割成字符串数组
 */
println("aa  bb cc dd".splitms()) // 返回["aa" "bb" "cc" "dd"]
println(" aa  bb cc dd".splitms()) // 返回["aa" "bb" "cc" "dd"]
println(" aa  bb   cc    dd  ".splitms()) // 返回["aa" "bb" "cc" "dd"]
println("-----------------------------------------------------------------------------")

/**
 * String.left(length)
 * String对象扩展方法
 * 返回字符串最左面length个字符
 */
println("abc".left( -1)) // 返回""
println("abc".left( 0)) // 返回""
println("abc".left(1)) // 返回"a"
println("abc".left(2)) // 返回"ab"
println("abc".left( 5)) // 返回"abc"
println("-----------------------------------------------------------------------------")

/**
 * String.right(length)
 * String对象扩展方法
 * 返回字符串最右面length个字符
 */
println("abc".right(-1)) // 返回""
println("abc".right( 0)) // 返回""
println("abc".right( 1)) // 返回"c"
println("abc".right( 2)) // 返回"bc"
println("abc".right( 5)) // 返回"abc"
println("-----------------------------------------------------------------------------")

/**
 * String.lpad(length, padstr)
 * String对象扩展方法
 * 使用字符串padstr对字符串最左边进行填充，直到长度为length个字符长度
 */
println("abc".lpad(-1, "*")) // 返回"abc"
println("abc".lpad(0, "*")) // 返回""
println("abc".lpad(1, "*")) // 返回"a"
println("abc".lpad(2, "*")) // 返回"ab"
println("abc".lpad(5, "*")) // 返回"**abc"
println("-----------------------------------------------------------------------------")

/**
 * String.rpad(length, padstr)
 * String对象扩展方法
 * 使用字符串padstr对字符串最右边进行填充，直到长度为length个字符长度
 */
println("abc".rpad( -1, "*")) // 返回"abc"
println("abc".rpad( 0, "*")) // 返回""
println("abc".rpad(1, "*")) // 返回"a"
println("abc".rpad(2, "*")) // 返回"ab"
println("abc".rpad(5, "*")) // 返回"abc**"
println("-----------------------------------------------------------------------------")

/**
 * join(array, sep)
 * 将数组中的元素连接成为一个字符串，之间用sep来分隔。
 *   array定义参考：数组中的元素类型为"字符串"、"数值"、"布尔值"和null
 *   sep:分隔符，缺省值为空格" "
 */
var arr = ["abc", 123, true, 88.1, null, 99.17]
println(join(arr))      // 返回 abc 123 true 88.1  99.17
println(join(arr, ",")) // 返回 abc,123,true,88.1,,99.17
println(join(arr, "|")) // 返回 abc|123|true|88.1||99.17

