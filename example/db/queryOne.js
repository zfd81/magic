var db = $.mysql("localhost", 3306, "root", "123456", "das")

//定义查询SQL
var sql = "SELECT \
                id, \
                name, \
                full_name, \
                phone_number, \
                email, \
                creator, \
                DATE_FORMAT(created_time, '%Y-%m-%d %H\\:%i\\:%s') AS created_time, \
                modifier, \
                DATE_FORMAT(modified_time, '%Y-%m-%d %H\\:%i\\:%s') AS modified_time \
            FROM \
                das_sys_user \
            WHERE \
                id = :val";

//查询单用户信息
db.queryOne(sql, "202004171900")
    .then(function (data) {
        println(data.name)
    })
    .catch(function (error) {
        println(error)
    });


//用户登录
db.queryOne("select * from das_sys_user where name=:name and password=:pwd", {
    name: "admin",
    pwd: "123456"
})
    .then(function (data) {
        if (data == null) {
            println("用户名或密码错误")
            return
        }
        println(data.name)
    })
    .catch(function (error) {
        println(error)
    });
