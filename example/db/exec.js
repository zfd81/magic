var db = $.mysql("localhost", 3306, "root", "123456", "das")

var user = {
    id: "1234567890",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    createdTime: new Date(),
    modifier: "123",
    modifiedTime: new Date()

}
var sql = " \
    insert into das_sys_user \
        (id,name,password,full_name,status,creator,created_time,modifier,modified_time) \
    values \
        (:id,:name,:password,:full_name,:status,:creator,:createdTime,:modifier,:modifiedTime)"

//通过exec方法添加用户，需要自己写SQL语句，user的key必须和SQL语句中“:变量”名相同
db.exec(sql, user)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });


//修改用户信息
db.exec("update das_sys_user set name=:name, password=:password, full_name=:fullName where id=:id", {
    id: "1234567890",
    name: "newUser",
    password: "999999999",
    fullName: "新用户"
})
    .then(function (data) {
        println("成功修改用户信息", data)
    })
    .catch(function (error) {
        println(error)
    });

//删除用户
db.exec("delete from das_sys_user where id=:id", user)
    .then(function (data) {
        println("成功删除用户", data)
    })
    .catch(function (error) {
        println(error)
    });

println("--------------------------------------------------")

var users = [{
    id: "12345678901",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    createdTime: new Date(),
    modifier: "123",
    modified_time: new Date()

}, {
    id: "12345678902",
    name: "user2",
    password: "88888888",
    full_name: "用户2",
    status: 1,
    creator: "123",
    createdTime: new Date(),
    modifier: "123",
    modified_time: new Date()

}]

sql = " \
    insert into das_sys_user \
        (id,name,password,full_name,status,creator,created_time,modifier,modified_time) \
    values \
        {@vals[,] (:this.id,:this.name,:this.password,:this.full_name,:this.status,:this.creator,:this.createdTime,:this.modifier,:this.modified_time)}"

//添加多用户
db.exec(sql, users)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });

//删除多用户
db.exec("delete from das_sys_user where {@vals[OR] id=:this.id}", users)
    .then(function (data) {
        println("成功删除用户", data)
    })
    .catch(function (error) {
        println(error)
    });

