var db = $.mysql("localhost", 3306, "root", "123456", "das")
var table = "das_sys_user" //表名
var user = {
    id: "1234567890",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    created_time: new Date(),
    modifier: "123",
    modified_time: new Date()
}
//通过save方法添加用户，要求user的key必须和das_sys_user的列名相同
db.save(table, user)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });

//删除用户
db.exec("delete from das_sys_user where id=:val", "1234567890")
    .then(function (data) {
        println("成功删除用户", data)
    })
    .catch(function (error) {
        println(error)
    });

var users = [{
    id: "12345678901",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    created_time: new Date(),
    modifier: "123",
    modified_time: new Date()

}, {
    id: "12345678902",
    name: "user2",
    password: "88888888",
    full_name: "用户2",
    status: 1,
    creator: "123",
    created_time: new Date(),
    modifier: "123",
    modified_time: new Date()

}]

//添加多用户
db.save(table, users)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });

//删除多用户
db.exec("delete from das_sys_user where {@vals[OR] id=:this.id}", users)
    .then(function (data) {
        println("成功删除用户", data)
    })
    .catch(function (error) {
        println(error)
    });
