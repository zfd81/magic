var db = $.mysql("localhost", 3306, "root", "123456", "das")

var pageNumber = 1 //页码，页码从1开始
var pageSize = 20 //每页显示的条数
//分页查询用户信息
db.query("select * from das_sys_user", null, pageNumber, pageSize)
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });

println("--------------------------------------------------")

var status = 1
var name = "ad"
//条件查询
db.query("select * from das_sys_user where status=:status and name like CONCAT(:name,'%')", {
    status: status,
    name: name
}, pageNumber, pageSize)
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });

println("--------------------------------------------------")

name = "%d%"
//条件查询，不需要翻页
db.query("select * from das_sys_user where status=:status and name like :name", {
    status: status,
    name: name
})
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });