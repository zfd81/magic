/**
 * fmkdir(name) 
 * 创建目录
 */
$.fmkdir("hello/world")//在当前位置创建目录
$.fmkdir("/root/hello/world")//在指定位置创建目录

/**
 * fread(name)
 * 一次读取文件全部内容
 */
var content = $.fread("README.md")
$.println(content)

/**
 * freadbyline(name, handler)
 * 逐行读取文件内容
 * handler返回true，终止文件读取
 */
//读取文件内容并添加行号显示
var index = 1
$.freadbyline("README.md", function (line) {
    $.printf("%d. %s \n", index, line)
    index++
})

//读取文件前5条非空数据
var cnt = 0
$.freadbyline("README.md", function (line) {
    if (cnt == 5) {
        return
    }
    if (line.trim() != "") {
        $.println(line)
        cnt++
    }
})

/**
 * fls(name[, reg])
 * 列出目录下的文件和子目录
 */
var files = $.fls("/root")
files.forEach((value) => {
    $.print(value.name, ":") //文件名
    $.print(value.isdir, ":") //是否是目录
    $.println(value.path) //文件路径
});

//查询文件名以'o'结尾的文件
files = $.fls("/root", "*o")
files.forEach(value => {
    $.println(value.name, ":", value.isdir, ":", value.path)
});
$.println("-------------------------------------")

//查询文件名以'.'开头的文件
files = $.fls("/root", "\\.*")
files.forEach(value => {
    $.println(value.name, ":", value.isdir, ":", value.path)
});
$.println("-------------------------------------")

//查询文件名以'h'开头的文件
files = $.fls("/root", "h*")
files.forEach(value => {
    $.println(value.name, ":", value.isdir, ":", value.path)
});
$.println("-------------------------------------")


//查询文件名包含'co'的文件
files = $.fls("/root", "*co*")
files.forEach(value => {
    $.println(value.name, ":", value.isdir, ":", value.path)
});
$.println("-------------------------------------")

/**
 * flsfile(name[, reg])
 * 列出目录下的文件
 */
var files = $.flsfile("/root")
files.forEach(value => {
    $.println(value.name, ":", value.isdir, ":", value.path)
});
$.println("-------------------------------------")

/**
* flsdir(name[, reg])
* 列出目录下的子目录
*/
var files = $.flsdir("/root")
files.forEach(value => {
    $.println(value.name, ":", value.isdir, ":", value.path)
});
$.println("-------------------------------------")
