/**
 * $.args 运行时参数，是一个字符串数组
 */
var size = $.args.length //获取运行参数的个数
var first = $.args[0] //获得第一个参数
println("参数个数为：", size)
println("第一个参数为：", first)