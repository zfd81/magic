package util

import "reflect"

func Map[P any, R any](slice []P, callback func(item P, index int) R) []R {
	newSlice := make([]R, 0)
	for i, val := range slice {
		item := callback(val, i)
		if reflect.ValueOf(item).Kind() == reflect.Invalid {
			continue
		}
		newSlice = append(newSlice, item)
	}
	return newSlice
}
