# Magic

## magic入门

magic是实现ECMAScript 5.1脚本引擎。

## magic函数
### string扩展函数

#### left(str, length) 
返回字符串str最左面length个字符

```js
left("abc", -1) // 返回""
left("abc", 0) // 返回""
left("abc", 2) // 返回"ab"
left("abc", 5) // 返回"abc"
```

#### right(str, length) 
返回字符串str最右面length个字符

```js
right("abc", -1) // 返回""
right("abc", 0) // 返回""
right("abc", 2) // 返回"bc"
right("abc", 5) // 返回"abc"
```

#### lpad(str, length, padstr) 
使用字符串padstr对字符串str最左边进行填充，直到长度为length个字符长度

```js
lpad("abc", -1, "*") // 返回"abc"
lpad("abc", 0, "*") // 返回""
lpad("abc", 2, "*") // 返回"ab"
lpad("abc", 5, "*") // 返回"**abc"
```

#### rpad(str, length, padstr) 
使用字符串padstr对字符串str最右边进行填充，直到长度为length个字符长度

```js
rpad("abc", -1, "*") // 返回"abc"
rpad("abc", 0, "*") // 返回""
rpad("abc", 2, "*") // 返回"ab"
rpad("abc", 5, "*") // 返回"abc**"
```

#### startsWith(str, prefix) 
检测字符串str是否以指定的前缀prefix开始

```js
startsWith("abc", "ab") // 返回true
startsWith("abc", "bc") // 返回false
```

#### endsWith(str, suffix) 
检测字符串str是否以指定的后缀suffix结束

```js
endsWith("abc", "ab") // 返回false
endsWith("abc", "bc") // 返回true
```

#### split(str, sep) 
通过分隔符sep把字符串str分割成字符串数组

```js
split("ab,cd,ef", ",") // 返回["ab" "cd" "ef"]
split("ab,cd,ef", "") // 返回["a" "b" "," "c" "d" "," "e" "f"]
split(",ab,cd,ef", ",") // 返回["" "ab" "cd" "ef"]
split("ab,cd,ef,", ",") // 返回["ab" "cd" "ef" ""]
```

#### splitn(str, sep, n) 
通过分隔符sep把字符串str分割成字符串数组,参数n决定返回的数组的数目  

-  n > 0: 返回的数组最多n个子字符串；最后一个子字符串包含未进行切割的部分
-  n == 0: 返回空数组
-  n < 0: 返回所有的子字符串组成的数组

```js
splitn("ab,cd,ef", ",", 5) // 返回["ab" "cd" "ef"]
splitn("ab,cd,ef", ",", 2) // 返回["ab" "cd,ef"]
splitn("ab,cd,ef", ",", 0) // 返回[]
splitn("ab,cd,ef", ",", -1) // 返回["ab" "cd" "ef"]
splitn("ab,cd,ef", "", 5) // 返回["a" "b" "," "c" "d,ef"]
splitn(",ab,cd,ef", ",", 5) // 返回["" "ab" "cd" "ef"]
splitn("ab,cd,ef,", ",", 2) // 返回["ab" "cd,ef,"]
```

#### splitms(str)
通过空格把字符串str分割成字符串数组，忽略空格个数，多空格视为一个空格

```js
splitms("aa  bb cc dd") // 返回["aa" "bb" "cc" "dd"]
splitms(" aa  bb cc dd") // 返回["aa" "bb" "cc" "dd"]
splitms(" aa  bb   cc    dd  ") // 返回["aa" "bb" "cc" "dd"]
```

#### join(array, sep) 
将数组中的元素连接成为一个字符串，之间用sep来分隔。
- array定义参考：数组中的元素类型为"字符串"、"数值"、"布尔值"和null
- sep:分隔符，缺省值为逗号","
```js
var arr = ["abc",123,true,88.1,null,99.17]
join(arr) // 返回 abc,123,true,88.1,,99.17
join(arr,"|") // 返回 abc|123|true|88.1||99.17
```

### 系统函数扩展函数

#### scan(prompt)
标准输入
- prompt：输入提示
```js
var name = scan("Please enter your name: ")
println("My name is ",name)
```

#### print(...args)
标准输出，内容之间不存在空格，输出没有换行
```js
print("January");
print(2, "February", 3, "March\n");
```

#### println(...args)
标准输出，内容之间存在空格，输出带有换行
```js
println("April");
println(5, "May", 6, "June");
```

#### printf(...args)
格式化输出
 - %s           字符串
 - %t           true 或 false
 - %b           二进制表示 
 - %c           相应Unicode码点所表示的字符 
 - %d           十进制表示 
 - %o           八进制表示 
 - %x           十六进制表示，字母形式为小写 a-f 
 - %X           十六进制表示，字母形式为大写 A-F 
 - %U           Unicode格式：123，等同于 "U+007B
 - %f, %g, %e   浮点数
 - %v           原值输出
```js
printf("I like %s.", "July") //格式化输出,不换行
printf("I'm %d years old. \n", 33) //格式化输出,换行\n
printf("I'm %s, %d years old. \n", "zfd", 42)
```

#### sleep(sec)
休眠函数，单位（秒）
```js
sleep(1);//程序执行过程休眠1秒
```


### 文件函数

#### fmkdir(name)
创建目录
```js
$.fmkdir("hello/world")//在当前位置创建目录
$.fmkdir("/root/hello/world")//在指定位置创建目录
```
#### fls(name[, reg])
列出目录下的文件和子目录
```js
var files = $.fls("/root")
files.forEach((value) => {
    print(value.name, ":") //文件名
    print(value.isdir, ":") //是否是目录
    println(value.path) //文件路径
});
```

```js
var files = $.fls("/root", "*o") //查询文件名以'o'结尾的文件
files.forEach(value => {
    println(value.name, ":", value.isdir, ":", value.path)
});
```

```js
var files = $.fls("/root", "\\.*") //查询文件名以'.'开头的文件
files.forEach(value => {
    println(value.name, ":", value.isdir, ":", value.path)
});
```

```js
var files = $.fls("/root", "h*") //查询文件名以'h'开头的文件
files.forEach(value => {
    println(value.name, ":", value.isdir, ":", value.path)
});
```

```js
var files = $.fls("/root", "*co*") //查询文件名包含'co'的文件
files.forEach(value => {
    println(value.name, ":", value.isdir, ":", value.path)
});
```

#### flsfile(name[, reg])
列出目录下的文件
```js
var files = $.flsfile("/root")
files.forEach(value => {
    println(value.name, ":", value.isdir, ":", value.path)
});
```

#### flsdir(name[, reg])
列出目录下的子目录
```js
var files = $.flsdir("/root")
files.forEach(value => {
    println(value.name, ":", value.isdir, ":", value.path)
});
```

#### fread(name)
一次读取文件全部内容
```js
var content = $.fread("README.md")
println(content)
```

#### freadbyline(name, handler)
逐行读取文件内容。handler返回true，终止文件读取
```js
//读取文件内容并添加行号显示
var index = 1
$.freadbyline("README.md", function (line) {
    printf("%d. %s \n", index, line)
    index++
})
```

```js
//读取文件前5条非空数据
var cnt = 0
$.freadbyline("README.md", function (line) {
    if (cnt == 5) {
        return
    }
    if (line.trim() != "") {
        println(line)
        cnt++
    }
})
```

#### fcopy(src, dest)
拷贝文件
```js
$.fcopy("README.md","example/dir/README.md")
```

#### frename(path, newname)
修改文件或目录的名字
```js
$.frename("example/dir/README.md","README_new.md") //修改文件名
$.frename("example/dir/test","test_new") //修改目录名
```

#### fremove(path)
删除文件或目录
```js
$.fremove("example/dir/README_new.md") //删除文件
$.fremove("example/dir/test_new") //删除目录
```

#### File对象
文件对象用来写文件操作

##### newFile(name)
创建文件对象
```js
var file = $.newFile("example/dir/newfile.txt")
```

##### writeString(str)
写入字符串
```js
var file = $.newFile("example/dir/newfile.txt")
for(i=0;i<10000000;i++){
    file.writeString("id"+i)
}
```

##### writeLine(str)
写入字符串并带有换行
```js
var file = $.newFile("example/dir/newfile.txt")
for(i=0;i<10000000;i++){
    file.writeLine("id"+i)
}
```

#### ftail(name, handler[, mustExist])
跟踪文件内容变化。handler返回true，终止文件跟踪
- mustExist：跟踪文件不存在是否报错，默认值为false
```js
var index = 1 //退出计数器
$.ftail("example/dir/README.md", function (line) {
    printf("%d. %s \n", index, line)
    index++
    if(index>5){
        return true
    }
})

//文件不存在报错
$.ftail("example/dir/must_exist.md", function (line) {
    println(line)
},true)
```

### Ftp操作

#### newftp(addr, user, password)
创建ftp连接
```js
var ftp = $.newftp("127.0.0.1:2231", "datacloud", "123456")
```

#### mkdir(rpath)
创建远程目录
```js
try {
    ftp.mkdir("/zfd/ftpfile/test")
} catch (error) {
    println("mkdir err:", error)
}
```

#### rmdir(rpath)
删除远程目录
```js
try {
    ftp.rmdir("/zfd/ftpfile/test")
} catch (error) {
    println("rmdir err:", error)
}
```

#### ls(rpath, reg) 
查看远程目录下的文件和子目录
```js
try {
    files = ftp.ls("/zfd")
    files.forEach(value => {
        println(value.name, ":", value.isdir,":",value.path)
    });
} catch (error) {
    println("err:", error)
}
```

#### lsfile(rpath, reg) 
查看远程目录下的文件
```js
try {
    files = ftp.lsfile("/zfd")
    files.forEach(value => {
        println(value.name, ":", value.isdir)
    });
} catch (error) {
    println("err:", error)
}
```
#### lsdir(rpath, reg)
查看远程目录下的子目录
```js
try {
    files = ftp.lsdir("/zfd")
    files.forEach(value => {
        println(value.name, ":", value.isdir,":",value.path)
    });
} catch (error) {
    println("err:", error)
}
```

```js
try {
    files = ftp.lsdir("/zfd", "V2.5*") //查询以'V2.5'开头的子目录
    files.forEach(value => {
        println(value.name, ":", value.isdir,":",value.path)
    });
} catch (error) {
    println("err:", error)
}
```

#### get(rfile, lfile)  
获得远程文件
```js
try {
    ftp.get("/zfd/ftpfile/webapps.tar.gz", "example/dir/webapps_new.tar.gz")
} catch (error) {
    println("err:", error)
}
```

#### put(lfile, rfile)  
上传本地文件
```js
try {
    ftp.put("README.md", "/zfd/ftpfile/read.md")
} catch (error) {
    println("err:", error)
}
```

#### rename(rfile, newname)  
修改远程文件或目录的名字
```js
try {
    ftp.rename("/zfd/ftpfile/read.md","read_new.md") //修改文件名
    ftp.rename("/zfd/ftpfile/test","test_new") //修改目录名
} catch (error) {
    println("rename err:", error)
}
```

#### delete(rfile) 
删除远程文件
```js
try {
    ftp.delete("/zfd/ftpfile/read_new.md")
} catch (error) {
    println("err:", error)
}
```

### SFtp操作

#### newsftp(addr, user, password)
创建Sftp连接
```js
var sftp = $.newsftp("127.0.0.1:2231", "datacloud", "123456")
```

#### mkdir(rpath)
创建远程目录
```js
try {
    sftp.mkdir("/zfd/ftpfile/test")
} catch (error) {
    println("mkdir err:", error)
}
```

#### rmdir(rpath)
删除远程目录
```js
try {
    sftp.rmdir("/zfd/ftpfile/test")
} catch (error) {
    println("rmdir err:", error)
}
```

#### ls(rpath, reg) 
查看远程目录下的文件和子目录
```js
try {
    files = sftp.ls("/zfd")
    files.forEach(value => {
        println(value.name, ":", value.isdir,":",value.path)
    });
} catch (error) {
    println("err:", error)
}
```

#### lsfile(rpath, reg) 
查看远程目录下的文件
```js
try {
    files = sftp.lsfile("/zfd")
    files.forEach(value => {
        println(value.name, ":", value.isdir)
    });
} catch (error) {
    println("err:", error)
}
```
#### lsdir(rpath, reg)
查看远程目录下的子目录
```js
try {
    files = sftp.lsdir("/zfd")
    files.forEach(value => {
        println(value.name, ":", value.isdir,":",value.path)
    });
} catch (error) {
    println("err:", error)
}
```

```js
try {
    files = sftp.lsdir("/zfd", "V2.5*") //查询以'V2.5'开头的子目录
    files.forEach(value => {
        println(value.name, ":", value.isdir,":",value.path)
    });
} catch (error) {
    println("err:", error)
}
```

#### get(rfile, lfile)  
获得远程文件
```js
try {
    sftp.get("/zfd/ftpfile/webapps.tar.gz", "example/dir/webapps_new.tar.gz")
} catch (error) {
    println("err:", error)
}
```

#### put(lfile, rfile)  
上传本地文件
```js
try {
    sftp.put("README.md", "/zfd/ftpfile/read.md")
} catch (error) {
    println("err:", error)
}
```

#### rename(rfile, newname)  
修改远程文件或目录的名字
```js
try {
    sftp.rename("/zfd/ftpfile/read.md","read_new.md") //修改文件名
    sftp.rename("/zfd/ftpfile/test","test_new") //修改目录名
} catch (error) {
    println("rename err:", error)
}
```

#### delete(rfile) 
删除远程文件
```js
try {
    sftp.delete("/zfd/ftpfile/read_new.md")
} catch (error) {
    println("err:", error)
}
```



### 数据库使用

#### 数据库对象
magic通过数据库对象来访问数据库，数据库对象创建方式如下：

##### mysql(host, port, user, password, dbName)
创建Mysql数据库访问对象。
```js
var db = $.mysql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象
```

##### mssql(host, port, user, password, dbName)
创建Mssql数据库访问对象。
```js
var db = $.mssql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象
```

##### newPgsql(host, port, user, password, dbName)
创建Postgresql数据库访问对象。
```js
var db = $.pgsql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象
```

#### 数据库操作
##### save(table, args)
数据插入，等同于sql中的insert。
- table：数据表名
- args：插入的数据，要求args的key必须和数据表的列名相同

```js
var db = $.newMysql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象
var table = "das_sys_user" //表名
var user = {
    id: "1234567890",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    created_time: new Date(),
    modifier: "123",
    modified_time: new Date()
}
//通过save方法添加用户，要求user的key必须和das_sys_user的列名相同
db.save(table, user)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });

var users = [{
    id: "12345678901",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    created_time: new Date(),
    modifier: "123",
    modified_time: new Date()

}, {
    id: "12345678902",
    name: "user2",
    password: "88888888",
    full_name: "用户2",
    status: 1,
    creator: "123",
    created_time: new Date(),
    modifier: "123",
    modified_time: new Date()

}]
//添加多用户
db.save(table, users)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });
```

##### queryOne(sql[, args])
查询单条记录。
- sql：查询语句
- args：查询条件，可忽略

```js
var db = $.newMysql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象

//将查询条件写到SQL语句中
db.queryOne("select * from das_sys_user where id='20210810'")
    .then(function (data) {
        println(data.name)//输出用户名
    })
    .catch(function (error) {
        println(error) //输出错误信息
    });

//单一变量查询，SQL中的变量名固定为val
db.queryOne("select * from das_sys_user where id=:val", "20210810")
    .then(function (data) {
        println(data.name)//输出用户名
    })
    .catch(function (error) {
        println(error) //输出错误信息
    });

//多变量查询，SQL中的变量名为args中的key
db.queryOne("select * from das_sys_user where id=:ID and status=:Status", {ID:"20210810" ,Status:1})
    .then(function (data) {
        println(data.name)//输出用户名
    })
    .catch(function (error) {
        println(error) //输出错误信息
    });
```

##### query(sql[, arg, pageNumber, pageSize])
查询多条记录，可进行分页查询。
- sql：查询语句
- args：查询条件，可忽略
- pageNumber：页码，从1开始，可忽略
- pageSize：页面容量，可忽略

```js
var db = $.newMysql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象

//查询所有用户
db.query("select * from das_sys_user")
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });

//查询名字以li开头，并且状态为1的所有用户
db.query("select * from das_sys_user where status=:stat and name like concat(:name,'%') order by created_time", {
        name: "li",
        stat: "1"
    })
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });


//分页查询名字以li开头的所有用户
db.query("select * from das_sys_user where name like concat(:val,'%') order by created_time", "li", 1, 30)
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });

var ids = ["20210807", "20210808", "20210809", "20210810"];
//通过or查询指定id的用户
db.query("select * from das_sys_user where {@vals[OR] id=:this.val}", ids)
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });

//通过in查询指定id的用户
db.query("select * from das_sys_user where id in({@vals[,] :this.val})", ids)
    .then(function (data) {
        data.forEach(value => {
            println(value.name)
        });
    })
    .catch(function (error) {
        println(error)
    });
```

##### exec(sql[, args])
执行insert update delete操作。
- sql：操作语句
- args：参数，可忽略

```js
var db = $.newMysql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象

var user = {
    id: "1234567890",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    createdTime: new Date(),
    modifier: "123",
    modifiedTime: new Date()

}
var sql = " \
    insert into das_sys_user \
        (id,name,password,full_name,status,creator,created_time,modifier,modified_time) \
    values \
        (:id,:name,:password,:full_name,:status,:creator,:createdTime,:modifier,:modifiedTime)"

//通过exec方法添加用户，需要自己写SQL语句，user的key必须和SQL语句中“:变量”名相同
db.exec(sql, user)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });

var users = [{
    id: "12345678901",
    name: "user1",
    password: "88888888",
    full_name: "用户1",
    status: 1,
    creator: "123",
    createdTime: new Date(),
    modifier: "123",
    modified_time: new Date()

}, {
    id: "12345678902",
    name: "user2",
    password: "88888888",
    full_name: "用户2",
    status: 1,
    creator: "123",
    createdTime: new Date(),
    modifier: "123",
    modified_time: new Date()

}]
sql = " \
    insert into das_sys_user \
        (id,name,password,full_name,status,creator,created_time,modifier,modified_time) \
    values \
        {@vals[,] (:this.id,:this.name,:this.password,:this.full_name,:this.status,:this.creator,:this.createdTime,:this.modifier,:this.modified_time)}"

//添加多用户
db.exec(sql, users)
    .then(function (data) {
        println("成功添加用户", data)
    })
    .catch(function (error) {
        println(error)
    });

//删除用户
db.exec("delete from das_sys_user where id=:val", "20210810")
    .then(function (data) {
        println("成功删除用户", data)
    })
    .catch(function (error) {
        println(error)
    });

//删除多用户
var ids = ["20210807", "20210808", "20210809", "20210810"];
db.exec("delete from das_sys_user where {@vals[OR] id=:this.val}", ids)
    .then(function (data) {
        println("成功删除用户", data)
    })
    .catch(function (error) {
        println(error)
    });

//修改用户信息
db.exec("update das_sys_user set name=:name, password=:password, full_name=:fullName where id=:id", {
    id: "1234567890",
    name: "newUser",
    password: "999999999",
    fullName: "新用户"
})
    .then(function (data) {
        println("成功修改用户信息", data)
    })
    .catch(function (error) {
        println(error)
    });

//修改多用户状态
db.exec("update das_sys_user set status='-1', modified_time=:modifiedTime where {@ids[OR] id=:this.val}", {
        modifiedTime: new Date(),
        ids: ["20210807", "20210808", "20210809", "20210810"]
    })
    .then(function (data) {
        $.resp.write(data)
    })
    .catch(function (error) {
        $.log.error(error); //打印日志
        $.resp.error(500, "用户修改失败");
    });
```

##### writeFile(sql, arg, name[, sep])
将查询结果写入文件
- sep：数据列分隔符，可选，默认值为','
```js
var db = $.newMysql("localhost", 3306, "root", "123456", "das") //返回数据库操作对象
try {
    var cnt = db.writeFile("select * from das_sys_user", null, "example/dir/das_sys_user.txt") //使用默认分隔符','
    printf("文件共%d条数据\n", cnt)

    cnt = db.writeFile("select * from das_sys_user", null, "example/dir/das_sys_user.txt", "|") //指定分隔符
    printf("文件共%d条数据\n", cnt)
} catch (e) {
    println(e)
}
```

