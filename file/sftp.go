package file

import (
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"time"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

type Sftp struct {
	client *sftp.Client
}

func (f *Sftp) Ls(rpath string) ([]map[string]interface{}, error) {
	files, err := f.client.ReadDir(rpath)
	if err != nil {
		return nil, err
	}
	items := []map[string]interface{}{}
	for _, file := range files {
		items = append(items, map[string]interface{}{
			"name":  file.Name(),
			"path":  f.client.Join(rpath, file.Name()),
			"isdir": file.IsDir(),
		})
	}
	return items, nil
}

func (f *Sftp) LsFunc(rpath string) []map[string]interface{} {
	files, err := f.Ls(rpath)
	if err != nil {
		panic(err)
	}
	return files
}

func (f *Sftp) Mkdir(rpath string) error {
	return f.client.MkdirAll(rpath)
}

func (f *Sftp) Rmdir(rpath string) error {
	return f.client.RemoveDirectory(rpath)
}

func (f *Sftp) Get(rfile, lfile string) error {
	srcFile, err := f.client.Open(rfile)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	destination, err := CreateFile(lfile)
	if err != nil {
		return err
	}
	defer destination.Close()

	_, err = srcFile.WriteTo(destination)

	// size := 1024 * 128
	// buf := make([]byte, size)
	// _, err = io.CopyBuffer(destination, srcFile, buf)

	return err
}

func (f *Sftp) Put(lfile, rfile string) error {
	sourceFileStat, err := os.Stat(lfile)
	if err != nil || sourceFileStat.IsDir() {
		return fmt.Errorf("open %s: No such file", lfile)
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", lfile)
	}

	source, err := os.Open(lfile)
	if err != nil {
		return err
	}
	defer source.Close()

	dir, _ := filepath.Split(rfile)
	f.Mkdir(dir)

	destination, err := f.client.Create(rfile)
	if err != nil {
		return err
	}
	defer destination.Close()

	size := 1024 * 128
	buf := make([]byte, size)
	_, err = io.CopyBuffer(destination, source, buf)
	return err
}

func (f *Sftp) Delete(rfile string) error {
	return f.client.Remove(rfile)
}

func (f *Sftp) Rename(rfile, name string) error {
	dir, _ := filepath.Split(rfile)
	return f.client.Rename(rfile, f.client.Join(dir, name))
}

func NewSftp(addr, user, password string) (*Sftp, error) {
	auth := []ssh.AuthMethod{ssh.Password(password)}
	auth = append(auth, ssh.Password(password))
	conf := &ssh.ClientConfig{
		User:    user,
		Auth:    auth,
		Timeout: 10 * time.Second,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},
	}
	conn, err := ssh.Dial("tcp", addr, conf)
	if err != nil {
		return nil, err
	}
	client, err := sftp.NewClient(conn)
	if err != nil {
		return nil, err
	}
	return &Sftp{
		client: client,
	}, nil
}
