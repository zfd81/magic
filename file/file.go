package file

import (
	"bufio"
	"fmt"
	"github.com/hpcloud/tail"
	"io"
	"os"
	"path/filepath"
	"time"
)

func FileCopy(src, dest string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}
	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := CreateFile(dest)
	if err != nil {
		return 0, err
	}
	defer destination.Close()

	size := 1024 * 128
	buf := make([]byte, size)
	return io.CopyBuffer(destination, source, buf)
}

func DirCopy(src, dest string) (int64, error) {
	var size int64
	items, err := os.ReadDir(src)
	if err != nil {
		return size, nil
	}
	for _, item := range items {
		if !item.IsDir() {
			nw, err := FileCopy(filepath.Join(src, item.Name()), filepath.Join(dest, item.Name()))
			if err != nil {
				return size, err
			}
			size += nw
			continue
		}

		err = os.MkdirAll(filepath.Join(dest, item.Name()), os.ModePerm)
		if err != nil {
			return size, err
		}

		nw, err := DirCopy(filepath.Join(src, item.Name()), filepath.Join(dest, item.Name()))
		if err != nil {
			return size, err
		}

		size += nw
	}
	return size, nil
}

func Copy(src, dest string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}
	if sourceFileStat.IsDir() {
		return DirCopy(src, dest)
	} else {
		return FileCopy(src, dest)
	}
}

func Rename(oldpath, newname string) error {
	path := filepath.Clean(oldpath)
	d, _ := filepath.Split(path)
	return os.Rename(oldpath, filepath.Join(d, newname))
}

func Remove(path string) error {
	return os.RemoveAll(path)
}

func Mkdir(name string) error {
	return os.MkdirAll(name, os.ModePerm)
}

func Ls(name string) ([]map[string]interface{}, error) {
	files, err := os.ReadDir(name)
	if err != nil {
		return nil, err
	}
	items := []map[string]interface{}{}
	for _, f := range files {
		// info, err := f.Info()
		// if err != nil {
		// 	return items, err
		// }
		items = append(items, map[string]interface{}{
			"name":  f.Name(),
			"path":  filepath.Join(name, f.Name()),
			"isdir": f.IsDir(),
		})
	}
	return items, nil
}

func ReadFile(name string) ([]byte, error) {
	stat, err := os.Stat(name)
	if err != nil || stat.IsDir() {
		return nil, fmt.Errorf("open %s: No such file", name)
	}
	return os.ReadFile(name)
}

func ReadFileByLine(name string, handler func(line string) bool) error {
	stat, err := os.Stat(name)
	if err != nil || stat.IsDir() {
		return fmt.Errorf("open %s: No such file", name)
	}

	file, err := os.Open(name)
	if err != nil {
		return fmt.Errorf("read file %s failed: %v", name, err.Error())
	}
	defer file.Close()

	dataStream := make(chan string, 2000)
	scanner := bufio.NewScanner(file)

	go func() {
		defer close(dataStream)
		for scanner.Scan() {
			dataStream <- scanner.Text()
		}
	}()

	for row := range dataStream {
		if handler(row) {
			return nil
		}
	}
	return nil
}

func IsExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		return os.IsExist(err)
	}
	return true
}

func CreateFile(name string) (*os.File, error) {
	path := filepath.Clean(name)
	dir, _ := filepath.Split(path)
	if !IsExist(dir) {
		os.MkdirAll(dir, os.ModePerm)
	}
	return os.Create(name)
}

type File struct {
	file   *os.File
	writer *bufio.Writer
}

func (f *File) Close() error {
	defer f.file.Close()
	err := f.writer.Flush()
	if err != nil {
		return fmt.Errorf("flush error :%v", err)
	}
	return nil
}

func (f *File) WriteString(s string) (int, error) {
	cnt, err := f.writer.WriteString(s)
	if err != nil {
		f.Close()
	}
	return cnt, err
}

func (f *File) WriteStringFunc(s string) int {
	cnt, err := f.writer.WriteString(s)
	if err != nil {
		panic(err)
	}
	return cnt
}

func (f *File) Write(bytes []byte) (int, error) {
	cnt, err := f.writer.Write(bytes)
	if err != nil {
		f.Close()
	}
	return cnt, err
}

func NewFile(name string) (*File, error) {
	f, err := CreateFile(name)
	if err != nil {
		return nil, fmt.Errorf("open file error: %v", err)
	}
	return &File{
		file:   f,
		writer: bufio.NewWriterSize(f, 1024*10),
	}, nil
}

func TailFile(name string, handler func(line string) bool, mustExist bool) error {
	config := tail.Config{
		ReOpen:    true,                                 // 重新打开
		Follow:    true,                                 // 是否跟随
		Location:  &tail.SeekInfo{Offset: 0, Whence: 2}, // 从文件的哪个地方开始读
		MustExist: mustExist,                            // 文件不存在不报错
		Poll:      true,
		Logger:    tail.DiscardingLogger,
	}
	tails, err := tail.TailFile(name, config)
	if err != nil {
		return fmt.Errorf("tail file %s error: %v", name, err)
	}
	var line *tail.Line
	var ok bool
	for {
		line, ok = <-tails.Lines //遍历chan，读取日志内容
		if !ok {
			time.Sleep(time.Second)
			continue
		}
		if handler(line.Text) {
			return nil
		}
	}
}
