package file

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/jlaffaye/ftp"
)

type Operation interface {
	Ls(rpath string) ([]map[string]interface{}, error)
	Mkdir(rpath string) error
	Rmdir(rpath string) error
	Get(rfile, lfile string) error
	Put(lfile, rfile string) error
	Delete(rfile string) error
	Rename(rfile, name string) error
}

type Ftp struct {
	client *ftp.ServerConn
}

func (f *Ftp) Ls(rpath string) ([]map[string]interface{}, error) {
	files, err := f.client.List(rpath)
	if err != nil {
		return nil, err
	}
	items := []map[string]interface{}{}
	for _, f := range files {
		items = append(items, map[string]interface{}{
			"name":  f.Name,
			"path":  filepath.Join(rpath, f.Name),
			"isdir": f.Type == ftp.EntryTypeFolder,
		})
	}
	return items, nil
}

func (f *Ftp) LsFunc(rpath string) []map[string]interface{} {
	files, err := f.Ls(rpath)
	if err != nil {
		panic(err)
	}
	return files
}

func (f *Ftp) Mkdir(rpath string) error {
	dir, base := filepath.Split(filepath.Clean(rpath))
	parts := strings.Split(filepath.Clean(dir), string(filepath.Separator))
	for _, part := range parts {
		f.client.MakeDir(part)
		f.client.ChangeDir(part)
	}
	defer func() {
		f.client.ChangeDir("/")
	}()
	return f.client.MakeDir(base)
}

func (f *Ftp) Rmdir(rpath string) error {
	return f.client.RemoveDir(rpath)
}

func (f *Ftp) Get(rfile, lfile string) error {
	resp, err := f.client.Retr(rfile)
	if err != nil {
		return err
	}
	defer resp.Close()

	destination, err := CreateFile(lfile)
	if err != nil {
		return err
	}
	defer destination.Close()

	size := 1024 * 128
	buf := make([]byte, size)
	_, err = io.CopyBuffer(destination, resp, buf)
	return err
}

func (f *Ftp) Put(lfile, rfile string) error {
	sourceFileStat, err := os.Stat(lfile)
	if err != nil || sourceFileStat.IsDir() {
		return fmt.Errorf("open %s: No such file", lfile)
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", lfile)
	}

	source, err := os.Open(lfile)
	if err != nil {
		return err
	}
	defer source.Close()

	dir, _ := filepath.Split(rfile)
	f.Mkdir(dir)
	return f.client.Stor(rfile, source)
}

func (f *Ftp) Delete(rfile string) error {
	return f.client.Delete(rfile)
}

func (f *Ftp) Rename(rfile, name string) error {
	dir, _ := filepath.Split(rfile)
	return f.client.Rename(rfile, filepath.Join(dir, name))
}

func NewFtp(addr, user, password string) (*Ftp, error) {
	conn, err := ftp.Dial(addr, ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		return nil, err
	}

	err = conn.Login(user, password)
	if err != nil {
		return nil, err
	}

	return &Ftp{
		client: conn,
	}, nil
}
