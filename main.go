package main

import (
	"fmt"
	"os"
	"path/filepath"

	"gitee.com/zfd81/magic/core"
)

func main() {
	length := len(os.Args)
	if length < 2 {
		fmt.Println("magic: no javascript files listed")
		return
	}

	name := os.Args[1]
	info, err := os.Stat(name)
	if err != nil || info.IsDir() {
		fmt.Printf("stat %s: no such file or directory \n", name)
		return
	}

	bytes, err := os.ReadFile(name)
	if err != nil {
		fmt.Printf("read file %s failed: %v \n", name, err)
		return
	}

	fullPath, err := filepath.Abs(name)
	if err != nil {
		fmt.Printf("get full path err: %v \n", err)
		return
	}

	js := core.NewEngine()
	js.SetRootPath(filepath.Dir(fullPath))
	js.SetVar("__magic_root", filepath.Dir(fullPath))
	js.SetVar("__magic_current_dir", filepath.Dir(fullPath))
	js.SetVar("__magic_current_program", filepath.Base(fullPath))
	js.SetVar("__magic_args", os.Args[2:])
	js.AddFunc("require", core.Require(js))
	js.SetScript(string(bytes))
	err = js.Run(nil)
	if err != nil {
		fmt.Printf("%s: %v \n", name, err)
		return
	}

}
